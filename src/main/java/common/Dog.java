package common;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * A Dog class created for the purpose of demonstrating 
 * the Apache Common Lang dependency, for simplified 
 * overwrite of toString().
 * <p>
 * Version 1.0 2019-06-10
 * <p>
 * Version 2.0 2019-06-10 Add content for overwrite of equals().
 * <p>
 * @author jay.loew
 * @version 2.0
 */
public class Dog {
	
	private String name;
	private int age;
	private long chipNumber;
	
	public Dog() { 
		System.out.println("A Dog is born.");
	}
	
	public Dog(String name) { 
		this.name = name;
		System.out.println("A dog named " + name + " is born.");
	}
	
	public void setAge(int age) { 
		if(age<0 || age > 20) { 
			throw new IllegalArgumentException("That's not a valid age for a dog.");
		} else { 
			this.age = age;
		}
	}
	public int getAge() { 
		return this.age;
	}
	
	public void setChipNumber(long chipNumber) { 
		if(chipNumber<100000) { 
			throw new IllegalArgumentException("That's not a valid chip ID.");
		} else { 
			this.chipNumber = chipNumber;
		}
	}
	public long getChipNumber() { 
		return this.chipNumber;
	}
	
	
	public void setName(String name) { 
		this.name = name;
	}
	public String getName() { 
		return this.name;
	}
	
	/**
	 * 
	 */
	@Override public String toString() { 
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
	
	/**
	 * Dog class assumes two dog instances with the same Chip IDs are in fact 
	 * the same animal.
	 */
	@Override public boolean equals(Object other) { 
		
		/*
		 * The equals() method takes an Object.
		 * The incoming Object could be anything.
		 * You must first determine whether it is a Dog.
		 * A not-Dog could never be equal to any Dog, 
		 * so return false immediately.
		 */
		if (! (other instanceof Dog) ) return false;
		
		/*
		 * You don't need to write any code yourself 
		 * for handling null. If the 'other' is null, 
		 * this method will return false, and you're 
		 * done. It's smart enough not to throw a 
		 * NullPointerException.
		 */
		
		/*
		 * Having determined that the incoming Object is a Dog, 
		 * you can now create a new, temporary Dog using explicit 
		 * casting. You will reference this other Dog in your 
		 * equality check.
		 */
		Dog otherDog = (Dog) other;
		
		/*
		 * Finally, perform the equality check that matters.
		 */
		return this.getChipNumber() == otherDog.getChipNumber();
	}
	
	
	/**
	 * The hashCode() method should use something in the class 
	 * that is likely to be unique and not likely to change.
	 * A Dog's chip number would likely be assigned for life.
	 */
	@Override public int hashCode() { 
		return (int) (7* this.getChipNumber());
	}
	
	
}
