// CAPITALIZE LETTERS
// Return a string with the first letter of every word capitalized
// ex. if String = "i love java", return "I Love Java"


package exercises;

/**
 * Demo: one way to change case of characters.
 * <p>
 * Use the static toUpperCase() method of the Character class.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 */
public class CapitalizeLetters {

	public static void main(String[] args) {

		char[] original = {'p','r','o','g','l','e','a','s','i','n','g'};

		System.out.print("original: ");
		for(char c : original) { 
			System.out.print(c + " ");
		}

		System.out.print("\ntoUpperCase(): ");
		for(char c : original) { 
			System.out.print(Character.toUpperCase(c) + " ");
		}

		System.out.println("\n======");

		String originalString = "original String";
		System.out.println(originalString.toUpperCase());

		System.out.println("======");
		System.out.println("begin 1st char only");

		String message = "Bonjour mes amis!";
		System.out.println("original: " + message);
		String[] tokens = message.split(" ");
		for(String S : tokens) { 
			if(Character.isLowerCase(S.charAt(0))) { 
				char toRaise = S.charAt(0);
				char raised = Character.toUpperCase(toRaise);
				S = S.replace(toRaise, raised);
			}
			System.out.print(S + " ");
		}


	}

}
