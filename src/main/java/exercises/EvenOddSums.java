// EVEN & ODD SUMS
// return an array of the sums of even and odd numbers
// ex.
// array = [50, 60, 60, 45, 71], return [170, 116]


package exercises;

import java.util.LinkedList;
import java.util.List;

/**
 * Good things to discuss about this one:
 * <ul>
 * <li>I don't print any result. Why not?</li>
 * <li>Index 0 contains the even and 1 contains the odds. Why not the other way around?</li>
 * <li>I suppress an 'unused' warning on the primary method call. Why?</li>
 * </ul>
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 */
public class EvenOddSums {
	
	private int[] originals = {1,2,3,4,5,6,7,8,9};

	private int[] sumsArray = new int[2];
	
	List<Integer> evens = new LinkedList<Integer>();
	
	List<Integer> odds = new LinkedList<Integer>();

	public int[] getSums() { 
		for(int i : originals) { 
			if( (i % 2) == 0) { 
				evens.add(i);
			} else { 
				odds.add(i);
			}
		}
		
		int sumEven = 0;
		for(int i : evens) { 
			sumEven += i;
		}
		sumsArray[0] = sumEven;
		
		int sumOdd = 0;
		for(int i : odds) { 
			sumOdd += i;
		}
		sumsArray[1] = sumOdd;
		
		return sumsArray;
	}

	public static void main(String[] args) {

		EvenOddSums sumsInstance = new EvenOddSums();
		int[] localSums = sumsInstance.getSums();
		for(int i : localSums) { 
			System.out.println(i);
		}
	}
}
