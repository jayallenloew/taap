package exercises;

/**
 * Capitalize the first letter of each word in a String.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 */
public class FirstLetterUp {

	public static void main(String[] args) {

		String original = "progressive";
		char[] cArray = original.toCharArray();
		cArray[0] = Character.toUpperCase(cArray[0]);
		for(char chars : cArray) { 
			System.out.print(chars + " ");
		}
		
		System.out.println("\n====");
		
		String message = "The Union Pacific Big Boy is really big.";
		String[] tokensOriginal = message.split(" ");
		for(String S : tokensOriginal) { 
			char[] chars = S.toCharArray();
			chars[0] = Character.toUpperCase(chars[0]);
			String temp = new String(chars);
			System.out.print(temp + " ");
		}
	}

}
