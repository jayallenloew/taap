// FIZZBUZZ
// Write a program that prints all the numbers from 1 to 100.
// For multiples of 3, instead of the number, print "Fizz", for multiples of 5 print "Buzz".
// For numbers which are multiples of both 3 and 5, print "FizzBuzz".


package exercises;

/**
 * The typical fizzbuzz interview question.
 * <p>
 * One solution is embedded if.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 */
public class FizzBuzz {
	
	public static void main(String[] args) {
		for(int i = 1;i<=100;i++) { 
			if(i%3==0) { 
				if(i%5==0) { // if both 3 and 5
					System.out.println(i + " FizzBuzz");
				}else { // if only 3
					System.out.println(i + " Fizz");
				}
			} else if(i%5==0){ // if only 5
				System.out.println(i + " Buzz");
			} else { // if neither 3 nor 5
				System.out.println(i);
			}
		}
	}



}
