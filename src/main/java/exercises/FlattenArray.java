// FLATTEN ARRAY
// Take an array of arrays and flatten to a single array
// ex. [[1, 2], [3, 4], [5, 6], [7]] returns [1, 2, 3, 4, 5, 6, 7]


package exercises;

import java.util.ArrayList;
import java.util.List;

/**
 * Turn a multi-dimensional array into a one-dim array.
 * <p>
 * Good things to discuss about this one:
 * <ul>
 * <li>Nothing in the requirement specifies the elimination of duplicates (so I didn't).</li>
 * <li>What if there was such a requirement? What else might you use?</li>
 * <li>Nothing in the requirement specifies output to console but I did anyway. Good idea or bad?</li>
 * </ul>
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 */
public class FlattenArray {

	public static void main(String[] args) {

		int[][] twoDimArray = new int[3][];
		twoDimArray[0] = new int[] { 1,2,3 };
		twoDimArray[1] = new int[] { 4,4,4,5,5 };
		twoDimArray[2] = new int[] { 6,7,8,9,11,12,13,14};

		List<Integer> listInts = new ArrayList<Integer>();
		int dimension1Size = twoDimArray.length;
		for(int j = 0; j < dimension1Size; j++) { 
			for(int i = 0; i < twoDimArray[j].length;i++) { 
				listInts.add(twoDimArray[j][i]);
			}
		}

		int[] oneDimArray = new int[listInts.size()];
		int index = 0;
		for(Integer I : listInts) { 
			oneDimArray[index] = I;
			index++;
		}
		for(int i : oneDimArray) { 
			System.out.print(i + " ");
		}
	}
}
