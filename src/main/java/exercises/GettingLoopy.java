package exercises;

/**
 * A demo class for various types of loops.
 * <p>
 * Get in the habit of using JavaDoc, both at  the class level (as in here) 
 * and at the method level (as below).
 * <p>
 * Version 1.0 2019-03-21 as per class.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 */
public class GettingLoopy {
	
	
	/**
	 * This class has a single, no-arguments constructor.
	 * <p>
	 * You don't have to pass any argument into this constructor to instantiate this class.
	 * <p>
	 * In fact, if you try to pass something in, you'll get an error.
	 * <p>
	 * After you've instantiated this class with a line like...
	 * <p>
	 * <pre>GettingLoopy getLoop = new GettingLoopy();</pre>
	 * <p>
	 * ...you'll see a message in the console: Hello from GettingLoopy constructor.
	 */
	public GettingLoopy() { 
		System.out.println("Hello from " + getClass().getSimpleName() + " constructor.");
	}
	
	
	/**
	 * This is the simplest possible example of a traditional for.
	 * <p>
	 * It doesn't loop through anything; it merely prints out the int value 
	 * that is declared and initialized inside of the array.
	 * <p>
	 * Understand the scope of any variable you declare.
	 */
	private void doSimplestTraditionalFor() { 
		for(int i=0;i<5;i++) { 
			System.out.println(i);
		}
		@SuppressWarnings("unused")
		final int i = 7; // bad name but no conflict of scope
		/*
		 * It's a bad idea to use single-letter variable names, generally speaking.
		 * The variable declared and initialized inside of the for loop is a justified exception.
		 * It lives and dies inside of the loop.
		 * It's won't be mistaken for anything else.
		 * There is no conflict with an identically-named variable outside the loop.
		 */
	}
	
	
	/**
	 * This array of String values is our subject for various loops that follow.
	 */
	private String[] teamMates = {"Brad","Shawn","Todd","Misty","Dallin"};
	
	
	/**
	 * A traditional for loop prints array elements.
	 * <p>
	 * Success depends on me correctly counting the number of elements--and 
	 * assumes the number of elements won't change often.
	 */
	private void doTradForThroughArray() { 
		for(int i=0;i<5;i++) { 
			System.out.println("teamMates[" + i + "]: " + teamMates[i]);
		}
	}
	
	
	/**
	 * A traditional for loop prints array elements, using the array's length 
	 * as its outer bound. No arithmetic required. No possibility of an out-of-bounds 
	 * scenario.
	 */
	private void doTradForWithLength() { 
		for(int i=0;i<teamMates.length;i++) { 
			System.out.println("teamMates with length: " + teamMates[i]);
		}
	}
	
	
	/**
	 * An enhanced for loop, also called a for-each, loops through all elements of 
	 * the array without arithmetic and regardless of array size.
	 * <p>
	 * Enhanced for loops are very popular.
	 * <p>
	 * They are very easy to use.
	 * <p>
	 * Use an enhanced for whenever you're sure you want every element in 
	 * the array, large or small.
	 */
	private void doEnahancedFor() { 
		for(String localVariableName : teamMates) { 
			System.out.println("teamMates enhanced for: " + localVariableName);
		}
	}
	
	/**
	 * This traditional while loop keeps on doing something while the 
	 * condition remains true. 
	 * <p>
	 * Once the condition becomes false, work stops.
	 * <p>
	 * Something inside the loop itself must eventually change the condition to false.
	 * Otherwise, the work goes on forever--an infinite loop. That's bad.
	 * <p>
	 * In theory, a traditional while loop may do no work at all. If the condition 
	 * is false to begin with, everything inside the loop is ignored, and the program 
	 * continues after the closing brace.
	 */
	private void doTraditionalWhile() { 
		int index = teamMates.length-1;
		while(index >= 0) { 
			System.out.println("teamMates while: " + teamMates[index]);
			index--;
		}
	}
	
	
	/**
	 * This do-while loop does something at least one time, 
	 * then evaluates a boolean condition to decide whether 
	 * to do it a 2nd time...to an nth time.
	 * <p>
	 * As it so happens, this begins at the first element, 
	 * then goes down to the last, inclusive.
	 * <p>
	 * Here too it's up to you to avoid an infinite loop: something inside the loop 
	 * itself must eventually cause the boolean expression to become false.
	 */
	private void doADoWhile() { 
		int index = 0;
		do { 
			System.out.println("teamMates do-while: " + teamMates[index]);
			index++;
		}while(index < teamMates.length);
	}
	
	
	/**
	 * The program's main method.
	 * <p>
	 * Run this program as a Java application.
	 * <p>
	 * @param args
	 */
	public static void main(String[] args) {
		
		GettingLoopy getLoop = new GettingLoopy();
		
		getLoop.doSimplestTraditionalFor();

		getLoop.doTradForThroughArray();
		
		getLoop.doTradForWithLength();

		getLoop.doEnahancedFor();
		
		getLoop.doTraditionalWhile();

		getLoop.doADoWhile();
	}

}
