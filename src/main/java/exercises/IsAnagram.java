// ANAGRAM
// Return true if anagram and false if not
// ex. String1 = "elbow", String2 = "below", returns true
// ex. String1 = "Dormitory", String2 = "dirty room", returns true


package exercises;

import java.util.ArrayList;
import java.util.List;

/**
 * Good things to discuss about this one:
 * <ul>
 * <li>Mike didn't specify case; does that omission (assumed intentional) change your solution?</li>
 * <li>Mike didn't specify any argument checking, but I added some. Good idea? Why or why not?</li>
 * <li>If your job was to write unit tests for this class, what would you come up with?</li>
 * </ul>
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 */
public class IsAnagram {
	
	public static boolean isAnagram(String arg1, String arg2) { 
		boolean isAnagram = false;
		if(arg1.trim().isEmpty() || arg2.trim().isEmpty()) { 
			throw new IllegalArgumentException("empty Strings cannot be anagrams");
		}
		if(arg1.trim().length()<3 || arg2.trim().length()<3) { 
			throw new IllegalArgumentException("try again with longer Strings");
		}
		char[] temp1 = arg1.toCharArray();
		char[] temp2 = arg2.toCharArray();
		List<Character> arg1List = new ArrayList<Character>();
		List<Character> arg2List = new ArrayList<Character>();
		for(char c : temp1) { 
			arg1List.add(c);
		}
		for(char c : temp2) { 
			arg2List.add(c);
		}
		temp1 = null;
		temp2 = null;
		if(arg1List.containsAll(arg2List)) { 
			isAnagram = true;
		}
		return isAnagram;
	}

	public static void main(String[] args) {
		
		System.out.println(isAnagram("dirty room", "Dormitory")); // false
		System.out.println(isAnagram("dirty room", "dormitory")); // true
		System.out.println(isAnagram("battle", "tablet")); // true; from m-w
	}

}
