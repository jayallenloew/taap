// LONGEST WORD
// Return the longest word of a string
// ex. if String = "Hi there, my name is Brad", return "there,"
// SOLUTION 1 - Return a single longest word
// SOLUTION 2 - Return an array and include multiple words if they have the same length
// SOLUTION 3 - Only return an array if multiple words, otherwise return a string

package exercises;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * Demo: one way to find length of longest word.
 * <p>
 * Use the static max() method of the Collections class.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 */
public class LongestWord {

	public static void main(String[] args) {
		
		List<Integer> lengths = new LinkedList<Integer>();

		ArrayList<String> names = new ArrayList<String>();
		names.add("Jessica");
		names.add("Deepa");
		names.add("Sivasubramanian");
		names.add("Courtney");
		names.add("Jeremy");
		names.add("Michael");
		names.add("Severine");
		names.add("Bonaparte");
		
		Iterator<String> iteratorString = names.iterator();
		int index = 0;
		String tempString = null;
		int tempLength = 0;
		while(iteratorString.hasNext()) { 
			tempString = iteratorString.next();
			tempLength = tempString.length();
			System.out.println("length of name at index " + index++ + ": " + tempLength + "\t(" + tempString + ")");
			lengths.add(tempLength);
		}
		
		System.out.println("======");
		
		System.out.println("Length of longest name: " + Collections.max(lengths));
	}

}
