// MAX CHARACTER
// Return the character that is most common in a string. If multiple letters have the same max count return an array.
// Only return an array if there is more then one result otherwise return a String
// ex. if String = "abracadabra", return 'a'
// ex. if String = "programming", return ['g', 'm', 'r']

package exercises;

import java.util.LinkedHashSet;

/**
 * Demo: Return count and identity of 'max characters' of a String 
 * defined as the character with the greatest number of occurrences 
 * in the given String.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 */
public class MaxCharacter {

	public static void main(String[] args) {
		
		// beginning String
		String toEvaluate = "abracadabra";
		System.out.println("to evaluate: " + toEvaluate);
		
		// primitive char array; all characters in the String
		char[] originalChars = toEvaluate.toCharArray();
		System.out.print("all chars: ");
		for(char c : originalChars) { 
			System.out.print(c + " ");
		}
		
		// collection; unique characters only
		LinkedHashSet<Character> uniqueChars = new LinkedHashSet<Character>();
		for(char c : originalChars) { 
			uniqueChars.add(c);
		}
		
		System.out.print("\nunique chars: ");
		for(Character C : uniqueChars) { 
			System.out.print(C + " ");
		}
		System.out.println("");
		
		int occurrence = 0;
		for(char c : uniqueChars) {
			for(char c2 : originalChars) { 
				if(c==c2) { 
					occurrence++;
				}
			}
			System.out.println("occurrence of \'" + c + "\': " + occurrence);
			occurrence = 0;
		}
	}
}
