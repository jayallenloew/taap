// MISSING LETTERS
// Find the missing letter in the passed letter range and return it. If all letters are present, return null
// ex.
// String = "abce", return "d"
// String = "abcdefghjklmno" return "i"
// String = "abcdefghijklmnopqrstuvwxyz" return null


package exercises;

/**
 * Rules:
 * <ul>
 * <li>Pass in an increasing sequence of letters, i.e. abcde...</li>
 * <li>Optionally skip one letter in sequence, i.e. abde...</li>
 * <li>Do not skip more than one letter in sequence.</li>
 * <li>Lower case.</li>
 * <li>No spaces.</li>
 * <li>No fewer than 3 letters.</li>
 * <li>Not null.</li>
 * </ul>
 * <p>
 * To discuss:
 * <ul>
 * <li>These are a lot of rules. In light of that, how could this solution be improved?</li>
 * <li>This solution in fact fails to meet one of the requirements. Which one?</li>
 * </ul>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 */
public class MissingLetters {

	public static void main(String[] args) {

		final String inputLocal = "abdefgijklmopqrsuvwxz"; // c h n t y
		
		for(int i = inputLocal.length()-1;i>0;i--) { 
			if(inputLocal.charAt(i) - inputLocal.charAt(i-1) == 1) { 
				continue;
			} else { 
				System.out.println("Missing: \'" + (char)(inputLocal.charAt(i)-1) + "\'");
			}
		}
	}
}
