// VALIDATE A PALINDROME
// Return true if string is a palindrome and false if not
// ex. "racecar" === true, "hello" == false


package exercises;

/**
 * I made this one into a standalone app, and added a proper test class for it.
 * <p>
 * See the test class, and critique it.
 * <p>
 * Good things to discuss about this one:
 * <ul>
 * <li>Mike didn't specify any argument checking, but I added some. Good idea? Why or why not?</li>
 * <li>I put the test in src/test/java. Good idea? Why or why not?</li>
 * </ul>
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 * @see src.test.java.PalindromeTests.java
 */
public class Palindrome {
	
	public Palindrome() { 
		// constructor
	}
	
	public boolean isPalindrome(String toEvaluate) {
		
		toEvaluate = toEvaluate.toLowerCase().trim();
		
		if(toEvaluate.length()<3) { 
			throw new IllegalArgumentException("try again, length >= 3");
		}
		for(char c : toEvaluate.toCharArray()) { 
			if(Character.isWhitespace(c)) { 
				throw new IllegalArgumentException("try again, no spaces");
			}
		}

		final char[] forward = toEvaluate.toCharArray();
		
		char[] backward = new char[forward.length];
		
		for(int i = (forward.length-1),j=0;i>=0;i--) { 
			backward[j++] = forward[i];
		}
		
		String fString = String.valueOf(forward);
		String bString = String.valueOf(backward);
		
		if(fString.contentEquals(bString)) { 
			return true;
		} else { 
			return false;
		}
	}
}
