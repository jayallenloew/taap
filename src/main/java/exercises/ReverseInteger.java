// REVERSE AN INTEGER
// Return an integer in reverse
// ex. if Int = 521, return 125 (not a string)

package exercises;

/**
 * Demo: Reverse an integer, and return the reversed value.
 * <p>
 * Use the StringBuilder class. 
 * <p>
 * But then use the static parseInt() method, because the requirement 
 * is to return an int, not a String.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 */
public class ReverseInteger {
	
	public static int getValueReversed(int valueIn) { 
		if(valueIn<10) { 
			throw new IllegalArgumentException("try again with at least a two-digit value");
		}
		StringBuilder sBuildOrig = new StringBuilder(String.valueOf(valueIn));
		return Integer.parseInt(sBuildOrig.reverse().toString());
	}
	
	public static void main(String[] args) {
		System.out.println(getValueReversed(100663)); // 366001
		System.out.println(getValueReversed(10)); // 1
		System.out.println(getValueReversed(100)); // 1
		System.out.println(getValueReversed(912)); // 219
	}



}
