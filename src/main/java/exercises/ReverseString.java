// REVERSE A STRING
// Return a string in reverse
// ex. if the string is 'hello' then 'olleh' is returned


package exercises;

/**
 * Demo: one way to reverse the characters of a String.
 * <p>
 * Use the StringBuffer class. 
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 */
public class ReverseString {
	
	public static void main(String[] args) {
				
		/*
		 * StringBuffer has overloaded constructors, one of 
		 * which takes a String literal. I call that constructor 
		 * here, passing in the original.
		 */
		StringBuffer stringBuff = new StringBuffer("progleasing");
				
		/*
		 * Having a StringBuffer instance to work with, I simply 
		 * call the .reverse() method of the StringBuffer class.
		 */
		System.out.println(stringBuff.reverse());
		
	}

}
