// SEEK & DESTROY
// Remove from the array whatever is in the second array. Return the leftover numbers in an array
// ex. if array1 = [2, 3, 4, 6, 6, "hello"], array2 = [2, 6], returns [3, 4, "hello"]


package exercises;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * To discuss:
 * <ul>
 * <li>Is the example a correct array?</li>
 * <li>What else might contain a collection of integers and Strings?</li>
 * <li>What if two arrays contain identical values?</li>
 * <li>Instructions assume the 1st array is the superset but what if that is not the case?</li>
 * <li>How could this solution be refactored for a mix of String and Integer?</li>
 * </ul>
 * <p>
 * @author jay.loew
 */
public class SeekAndDestroy {

	public static void main(String[] args) {
		int[] alpha = {91,2,0,44,45,100};
		int[] bravo = {40,2,100,91};
		List<Integer> Alpha = new ArrayList<Integer>();
		for(int i : alpha) { 
			Alpha.add(i);
		}
		List<Integer> Bravo = new ArrayList<Integer>();
		for(int i : bravo) { 
			Bravo.add(i);
		}
		Iterator<Integer> iterate = Bravo.iterator();
		while(iterate.hasNext()) { 
			Alpha.remove(iterate.next());
		}
		System.out.print("Leftovers: ");
		for(Integer I : Alpha) { 
			System.out.print(I + " ");
		}
	}

}
