// SUM ALL PRIMES
// Pass in a number to loop up to and add all of the prime numbers.
// A prime number is a whole number greater than 1 whose only factors are 1 and itself
// ex. if num = 10, return 17
package exercises;

/**
 * Demo: one way to sum primes up to an outer bound.
 * <p>
 * Use embedded loops.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 */
public class SumAllPrimes {

	public boolean isPrime(int toTest) { 
		// less than 1 don't bother
		if(toTest<1) { 
			throw new IllegalArgumentException("try again with 1 or greater");
		}
		// 1 is mathematically definitional; no code needed; just return true
		if(toTest==1) {
			return true;
		}
		// 2 we all know; it's the prime number that is even
		if(toTest==2) { 
			return true;
		}
		// if divisible by 2 it cannot possibly be prime
		if((toTest%2)==0) { 
			return false;
		}
		// now begin checking at 3
		for(int i = 3;i*i<=toTest;i+=2) { 
			if(toTest%i==0) { 
				return false;
			}
		}
		return true;
	}
	
	public int getSumsOfPrimesUpTo(int outerBound) { 
		int toReturn = 0;
		for(int i = 1;i<outerBound;i++) { 
			if(isPrime(i)) { 
				System.out.println("adding " + i);
				toReturn += i;
			}
		}
		return toReturn;
	}
	
	public static void main(String[] args) {
		SumAllPrimes sap = new SumAllPrimes();
		final int toTest = 19;
		System.out.println(toTest + " " + sap.isPrime(toTest));
		System.out.println("===");
		System.out.println("sum: " + sap.getSumsOfPrimesUpTo(toTest));
	}

}
