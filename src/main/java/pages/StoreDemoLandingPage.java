/*
 * Copyright Progressive Leasing LLC.
 */
package pages;

import java.util.HashSet;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utilities.WebElementGetter;

/**
 * Page object for <a href="http://storedemo.progressivelp.com/">store demo landing page</a> 
 * <p>
 * There is almost nothing on this page that we're responsible for, so this page object 
 * is very small.
 * <p>
 * Furthermore this page object is just an entry point for us. It's a step toward 
 * the stuff we care about.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @author <a href="mailto:christopher.rains@progleasing.com">CJ Rains</a>
 * @version 1.0 2019-01-04
 * @see StoreDemoLandingPageTests.java
 */
public class StoreDemoLandingPage {
	
	private WebDriver driverIn;
	
	public StoreDemoLandingPage(WebDriver driverIn) { 
		this.driverIn = driverIn;
	}
	
	/**
	 * Fetch an instance of org.openqa.selenium.WebElement representing the 'Find out more' button 
	 * at bottom: "No Credit Needed <BR> Find out more"
	 * <p>
	 * This supports a typical (not required) clickstream for testers: land here on the 
	 * store demo page, click Find out more to enter the Progressive flow.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getButtonFindOutMore() { 
		
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();
		
		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector("#prog-valprop > img"));
		locators.add(By.xpath("//a[@id='prog-valprop']/img"));
		locators.add(By.xpath("//img[@alt='Learn more about Progressive Leasing']"));
		locators.add(By.xpath("//p[3]/a/img"));
		
		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}


}
