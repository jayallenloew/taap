package taap.common;

/**
 * A simple example from week 01.
 * <p>
 * This class has no main() method. Rather, it has a single 
 * void method that prints the greeting.
 * <p>
 * In your test class, instantiate this class, then call the 
 * method via the object reference.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 * @see scr.test.java.taap.common.HelloWorldTest.java
 */
public class HelloWorld {
	
	public void sayHello() { 
		System.out.println("Bonjour Monde");
	}
	
}
