/*
 * Copyright Progressive Leasing LLC.
 */
package utilities;

import java.io.File;

/**
 * Commonly referenced resources for local test execution.
 * <p>
 * Change these values as you configure your machine.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 */
public class LocalResources {

	/**
	 * Root path to your local repo.
	 * <p>
	 * You must update this one time according to where you
	 * have cloned down this repo on your local machine.
	 * <p>
	 * I have left two recent examples from my own usage in here,  
	 * one WinTel and one Mac. 
	 * <p>
	 * @return A String representing the base directory.
	 */
	public static final String getPathToLocalRepo() { 
		
		// WinTel example
//		return "C:" + File.separator +
//				"git-repositories" + File.separator + 
//				"ecommerce.frontendtest" + File.separator; 

		// MacOS example
		return File.separator + 
				"Users" + 
				File.separator + 
				"first.last" +
				File.separator + 
				"git-repositories" + 
				File.separator + 
				"ecommerce.frontendtest" + 
				File.separator;
	}

	
	public static final String getPathToSrcTestResources() { 
		return LocalResources.getPathToLocalRepo() + 
				"src" + 
				File.separator + 
				"test" + 
				File.separator + 
				"resources" + 
				File.separator;
	}

	
	/**
	 * On your Mac, it is customary to let the installer put the 
	 * Firefox binary in /Applications. This method assumes that.
	 * <p>
	 * @return String representing full path to a Mac binary.
	 */
	public static final String getPathChromeDriverMac() { 
		return File.separator + 
				"usr" + 
				File.separator + 
				"local" + 
				File.separator + 
				"bin" + 
				File.separator + 
				"chromedriver";
	}



	
	/**
	 * Reminder, you need Geckodriver for Firefox after Selenium 3.
	 * <p>
	 * @return String representing full path to a Mac binary.
	 */
	public static final String getPathGeckoDriverMac() { 
		return File.separator + 
				"usr" + 
				File.separator + 
				"local" + 
				File.separator + 
				"bin" + 
				File.separator + 
				"geckodriver";
	}
	
	
	/**
	 * On WinTel, It is customary to override the installer's default and 
	 * relocate the binary to src/test/resources. This method assumes that.
	 * <p>
	 * @return String representing full path to a WinTel binary.
	 */
	public static final String getPathGeckoDriverWindows() { 
		return getPathToSrcTestResources() + "geckodriver.exe";
	}

	
	/**
	 * On WinTel, It is customary to override the installer's default and 
	 * relocate the binary to src/test/resources. This method assumes that.
	 * <p>
	 * @return String representing full path to a WinTel binary.
	 */
	public static final String getPathChromeDriverWindows() { 
		return  getPathToSrcTestResources() + "chromedriver.exe";
	}
}
