/*
 * Copyright Progressive Leasing LLC.
 */
package utilities.encryption;

import java.io.UnsupportedEncodingException;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import java.util.Arrays;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 * Run this class as a Java application.
 * <p>
 * Simple POJO helper for creating an encrypted key, which you 
 * can subsequently use for decryption if you like.
 * <p>
 * @author jay.loew
 * @version 1.0 2019-01-03
 */
public class A_CreateMyKey {
	
	/**
	 * Temporarily change this to the password you want to use to encrypt/decrypt.
	 * <p>
	 * This could be the same as your LDAP password, or it could be different.
	 * <P>
	 * In any case, its encrypted equivalent gets printed to console.
	 * <p>
	 * WARNING: do not persist your change! 
	 */
	private String oneTimeEncryptionPassword = "passwordToEncrypt";

	/**
	 * Internal. System-generated. Don't overwrite.
	 */
	private static SecretKeySpec secretKey;

	/**
	 * Internal. System-generated. Don't overwrite.
	 */
	private static byte[] keyByteArray;

	/**
	 * Don't mess with this either.
	 * <p>
	 * @param myKey
	 */
	public static void setMyKey(String myKey) {

		if(myKey.trim().length() < 7) { 
			throw new IllegalArgumentException("invalid length");
		}
		MessageDigest sha = null;
		try { 
			keyByteArray = myKey.getBytes("UTF-8");
			sha = MessageDigest.getInstance("SHA-1");
			keyByteArray = sha.digest(keyByteArray);
			keyByteArray = Arrays.copyOf(keyByteArray,16);
			secretKey = new SecretKeySpec(keyByteArray,"AES");
		}catch(NoSuchAlgorithmException nSAE) { 
			nSAE.printStackTrace();
		}catch(UnsupportedEncodingException uEE) { 
			uEE.printStackTrace();
		}
	}

	/**
	 * Encrypt the password you set at the top of this class, and print the encrypted value to console.
	 * <p>
	 * @param toEncrypt
	 */
	public void printMyEncryptedKey(String toEncrypt) { 
		try { 
			setMyKey(toEncrypt);
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			System.out.println(Base64.getEncoder().encodeToString(cipher.doFinal(toEncrypt.getBytes("UTF-8"))));
		} catch (Exception eX) {
			System.out.println("Error while encrypting");
			eX.printStackTrace();
		}
	}

	/**
	 * Instantiate the class, then print your new key to Console.
	 * <p>
	 * Do not pass in arguments.
	 * <p>
	 * @param args
	 */
	public static void main(String[] args) {
		
		A_CreateMyKey create = new A_CreateMyKey();
		
		create.printMyEncryptedKey(create.oneTimeEncryptionPassword);

	}

}
