package utilities.saucelabs;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import common.SauceLabsConstants;

/**
 * Simplify driver instantiation for tests that will run on physical 
 * mobile devices in the Sauce Labs cloud.
 * <p>
 * Version 1.0 - 2019-05-21
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a> 
 * @version 1.0
 */
public class CommonDriverSauceDevice {

	private RemoteWebDriver driverToReturn;				// Selenium
	private DesiredCapabilities dCaps;					// Selenium and Sauce Labs
	private DriverTypeSauce driverTypeSauce;			// Sauce Labs and Progressive
	private String nameOfCallingTest;	// all you :-)

	/**
	 * Select a driver type from the enum: which OS-browser combination 
	 * do you want for the test you are about to run? (These options can 
	 * and will change over time.)
	 * <p>
	 * Select a data center: USA or EU?
	 * <p>
	 * NOTE: For physical devices, there is one correct option: USA_RD.
	 * <p>
	 * Pass in a free-form String value (heads up, it's unchecked) 
	 * representing the name of your tunnel. The prepared driver you 
	 * get from this class will push your test through this tunnel, 
	 * so make sure the tunnel is open first.
	 * <p>
	 * @param driverType - An element from the enum for driver type.
	 * @param dataCenter - An element from the enum for data center.
	 * @param nameOfYourTunnel - An unchecked String: the name of your tunnel.
	 */
	public CommonDriverSauceDevice(DriverTypeSauce driverType) { 

		this.driverTypeSauce = driverType;

		instantiateDesiredCapabilities();
		authenticateSauceLabs();
		setTunnelSauceLabs();
		setSauceLabsGlobalTimeouts();

	}

	/**
	 * Sauce Labs uses the DesiredCapabilities class to set numerous 
	 * required and optional values.
	 */
	private void instantiateDesiredCapabilities() { 
		this.dCaps = new DesiredCapabilities();
	}

	/**
	 * Your Sauce Labs username and access key in system variables.
	 */
	private final void authenticateSauceLabs() { 
		dCaps.setCapability("username", System.getenv("SAUCE_USERNAME"));
		dCaps.setCapability("accessKey", System.getenv("SAUCE_ACCESS_KEY"));
		dCaps.setCapability("testobject_api_key", System.getenv("TESTOBJECT_API_KEY")); // begin 985		

		//		 for troubleshooting:
		//		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();
		//		System.out.println("\n");
		//		System.out.println(methodNameLocal + ":\tusername:" + dCaps.getCapability("username"));
		//		System.out.println(methodNameLocal + ":\taccessKey:" + dCaps.getCapability("accessKey"));
		//		System.out.println(methodNameLocal + ":\ttestobject_api_key:" + dCaps.getCapability("testobject_api_key"));
		//		System.out.println("\n");
	}


	/**
	 * This Sauce Labs test will try to run in this tunnel, so make 
	 * sure it's open first.
	 * <p>
	 * If this tunnel isn't open when your test begins, your test 
	 * will fail from the setup method in a recognizable way. Start 
	 * up your tunnel, and try again.
	 */
	private final void setTunnelSauceLabs() { 
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		dCaps.setCapability("tunnelIdentifier", System.getenv("SAUCE_TUNNEL_RD"));
		System.out.println("\n" + methodNameLocal + " : " + "your tunnel is " + dCaps.getCapability("tunnelIdentifier") + "\n");
	}

	/**
	 * 
	 * @param nameOfCallingTest - The test name will appear in your Dashboard.
	 */
	public void setNameOfCallingTest(String nameOfCallingTest) { 
		this.nameOfCallingTest = nameOfCallingTest;
	}
	public String getNameOfCallingTest() { 
		return this.nameOfCallingTest;
	}


	/**
	 * The next three values are defensive, and appropriate for most of our testing. 
	 * <p>
	 * These values can be overridden, but please be careful. And do it on your 
	 * own branch. 
	 * <p>
	 * Whatever these values are set to, they will apply to all tests running 
	 * in Sauce Labs.
	 * <p>
	 * See the JavaDoc for these values in SauceLabsConstants.java.
	 * <p>
	 * @see src/main/java/common/SauceLabsConstants.java
	 */
	private final void setSauceLabsGlobalTimeouts() { 
		dCaps.setCapability("commandTimeout", SauceLabsConstants.COMMAND_TIMEOUT);
		dCaps.setCapability("idleTimeout", SauceLabsConstants.IDLE_TIMEOUT);
		dCaps.setCapability("maxDuration", SauceLabsConstants.MAX_DURATION);		
	}

	/**
	 * Call this method to return a ready-to-use driver instance.
	 * <p>
	 * From within your test, do something like this:
	 * <p>
	 * <pre>
	 * driver = new CommonDriversSauce(DriverType.WINDOWS_CHROME, DataCenter.USA, "AllenTunnel").getPreparedDriver(method.getName());
	 * </pre>
	 * @param getNameOfCallingTest() - The test name will appear in your Dashboard.
	 * @return - An instance of AppiumDriver configured for immediate use in a test.
	 */
	public RemoteWebDriver getPreparedDriver() { 

		switch(this.driverTypeSauce) { 

		case IPHONE_X_RD : { 
			/*
			 * Name of currently running test will appear in Sauce Labs Dashboard.
			 */
			dCaps.setCapability("name", this.getNameOfCallingTest());

			dCaps.setCapability("deviceName","iPhone X");
			dCaps.setCapability("deviceOrientation", "portrait");
			dCaps.setCapability("platformVersion","12.1.4");
			dCaps.setCapability("platformName", "iOS");

			URL toDeviceCloud = null;
			try { 
				toDeviceCloud = new URL(SauceLabsConstants.URL_DEVICE_CLOUD_USA_RD);
			}catch(MalformedURLException mUE) { 
				throw new IllegalStateException(mUE);
			}

			driverToReturn = new RemoteWebDriver(toDeviceCloud,dCaps);

			break;
		} // end case


		case IPHONE_6SP_RD : { 
			/*
			 * Name of currently running test will appear in Sauce Labs Dashboard.
			 */
			dCaps.setCapability("name", this.getNameOfCallingTest());

			dCaps.setCapability("deviceName","iPhone 6S Plus"); // iPhone_6_Plus_12_real
			dCaps.setCapability("deviceOrientation", "portrait");
			dCaps.setCapability("platformVersion","12.1.4");
			dCaps.setCapability("platformName", "iOS");

			URL toDeviceCloud = null;
			try { 
				toDeviceCloud = new URL(SauceLabsConstants.URL_DEVICE_CLOUD_USA_RD);
			}catch(MalformedURLException mUE) { 
				throw new IllegalStateException(mUE);
			}

			driverToReturn = new RemoteWebDriver(toDeviceCloud,dCaps);

			break;
		} // end case
		


		case IPHONE_7_RD : { 
			/*
			 * Name of currently running test will appear in Sauce Labs Dashboard.
			 */
			dCaps.setCapability("name", this.getNameOfCallingTest());

			dCaps.setCapability("deviceName","iPhone 7"); 
			dCaps.setCapability("deviceOrientation", "portrait");
			dCaps.setCapability("platformVersion","12.2");
			dCaps.setCapability("platformName", "iOS");

			URL toDeviceCloud = null;
			try { 
				toDeviceCloud = new URL(SauceLabsConstants.URL_DEVICE_CLOUD_USA_RD);
			}catch(MalformedURLException mUE) { 
				throw new IllegalStateException(mUE);
			}

			driverToReturn = new RemoteWebDriver(toDeviceCloud,dCaps);

			break;
		} // end case
		


		case IPHONE_8_RD : { 
			/*
			 * Name of currently running test will appear in Sauce Labs Dashboard.
			 */
			dCaps.setCapability("name", this.getNameOfCallingTest());

			dCaps.setCapability("deviceName","iPhone 8"); 
			dCaps.setCapability("deviceOrientation", "portrait");
			dCaps.setCapability("platformVersion","12.1.4");
			dCaps.setCapability("platformName", "iOS");

			URL toDeviceCloud = null;
			try { 
				toDeviceCloud = new URL(SauceLabsConstants.URL_DEVICE_CLOUD_USA_RD);
			}catch(MalformedURLException mUE) { 
				throw new IllegalStateException(mUE);
			}

			driverToReturn = new RemoteWebDriver(toDeviceCloud,dCaps);

			break;
		} // end case
		
		
		case IPAD_MINI_2019_RD : { 
			/*
			 * Name of currently running test will appear in Sauce Labs Dashboard.
			 */
			dCaps.setCapability("name", this.getNameOfCallingTest());

			dCaps.setCapability("deviceName","iPad mini 2019"); 
			dCaps.setCapability("deviceOrientation", "portrait");
			dCaps.setCapability("platformVersion","12.2");
			dCaps.setCapability("platformName", "iOS");

			URL toDeviceCloud = null;
			try { 
				toDeviceCloud = new URL(SauceLabsConstants.URL_DEVICE_CLOUD_USA_RD);
			}catch(MalformedURLException mUE) { 
				throw new IllegalStateException(mUE);
			}

			driverToReturn = new RemoteWebDriver(toDeviceCloud,dCaps);

			break;
		} // end case
		

		
		case GLXY_NOTE_9_RD : { 
			/*
			 * Name of currently running test will appear in Sauce Labs Dashboard.
			 */
			dCaps.setCapability("name", this.getNameOfCallingTest());

			dCaps.setCapability("deviceName","Samsung Galaxy Note 9"); 
			dCaps.setCapability("deviceOrientation", "portrait");
			dCaps.setCapability("platformVersion","8.1.0");
			dCaps.setCapability("platformName", "Android");

			URL toDeviceCloud = null;
			try { 
				toDeviceCloud = new URL(SauceLabsConstants.URL_DEVICE_CLOUD_USA_RD);
			}catch(MalformedURLException mUE) { 
				throw new IllegalStateException(mUE);
			}

			driverToReturn = new RemoteWebDriver(toDeviceCloud,dCaps);

			break;
		} // end case
		
		

		
		case GLXY_S10e_RD : { 
			/*
			 * Name of currently running test will appear in Sauce Labs Dashboard.
			 */
			dCaps.setCapability("name", this.getNameOfCallingTest());

			dCaps.setCapability("deviceName","Samsung Galaxy S10e"); 
			dCaps.setCapability("deviceOrientation", "portrait");
			dCaps.setCapability("platformVersion","9");
			dCaps.setCapability("platformName", "Android");

			URL toDeviceCloud = null;
			try { 
				toDeviceCloud = new URL(SauceLabsConstants.URL_DEVICE_CLOUD_USA_RD);
			}catch(MalformedURLException mUE) { 
				throw new IllegalStateException(mUE);
			}

			driverToReturn = new RemoteWebDriver(toDeviceCloud,dCaps);

			break;
		} // end case	

		
		case GGL_PXL_3_XL_RD : { 
			/*
			 * Name of currently running test will appear in Sauce Labs Dashboard.
			 */
			dCaps.setCapability("name", this.getNameOfCallingTest());

			dCaps.setCapability("deviceName","Google Pixel 3 XL"); 
			dCaps.setCapability("deviceOrientation", "portrait");
			dCaps.setCapability("platformVersion","9");
			dCaps.setCapability("platformName", "Android");

			URL toDeviceCloud = null;
			try { 
				toDeviceCloud = new URL(SauceLabsConstants.URL_DEVICE_CLOUD_USA_RD);
			}catch(MalformedURLException mUE) { 
				throw new IllegalStateException(mUE);
			}

			driverToReturn = new RemoteWebDriver(toDeviceCloud,dCaps);

			break;
		} // end case
		
		
		
		default : { 
			System.out.println("Not supported yet. Work underway. Check back later.");
			break;
		}
		
		}
		return driverToReturn;
	}
}



