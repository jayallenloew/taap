package utilities.saucelabs;

import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import common.SauceLabsConstants;

/**
 * Simplify driver instantiation for tests that will run in the 
 * Sauce Labs cloud on mobile
 * <p>
 * Version 1.0 - 2019-05-10
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a> 
 * @version 1.0
 * @see utilities.saucelabs.CommonDriversSauceTests.java
 * @see common.SauceLabsConstants.java
 */
public class CommonDriverSauceMobile {

	private WebDriver driverToReturn;	// Selenium
	private DesiredCapabilities dCaps;	// Selenium and Sauce Labs
	private DriverTypeSauce driverTypeSauce;	// Sauce Labs and Progressive
	private DataCenter dataCenter;	// Sauce Labs
	private String nameOfCallingTest = "not yet set"; // all you :-)

	/**
	 * Select a driver type from the enum: which OS-browser combination 
	 * do you want for the test you are about to run? (These options can 
	 * and will change over time.)
	 * <p>
	 * Select a data center: USA or EU?
	 * <p>
	 * Pass in a free-form String value (heads up, it's unchecked) 
	 * representing the name of your tunnel. The prepared driver you 
	 * get from this class will push your test through this tunnel, 
	 * so make sure the tunnel is open first.
	 * <p>
	 * @param driverType - An element from the enum for driver type.
	 * @param dataCenter - An element from the enum for data center.
	 * @param nameOfYourTunnel - An unchecked String: the name of your tunnel.
	 */
	public CommonDriverSauceMobile(DriverTypeSauce driverType, DataCenter dataCenter) { 

		this.driverTypeSauce = driverType;
		this.dataCenter = dataCenter;

		instantiateDesiredCapabilities();
		authenticateSauceLabs();
		setSauceLabsGlobalTimeouts();
		setTunnelSauceLabs();

	}

	/**
	 * Sauce Labs uses the DesiredCapabilities class to set numerous 
	 * required and optional values.
	 */
	private void instantiateDesiredCapabilities() { 
		if(this.driverTypeSauce.toString().contains("IPHONE")) { 
			dCaps = new DesiredCapabilities().iphone();
		}
		if (this.driverTypeSauce.equals(DriverTypeSauce.SAMSUNG_GALAXY_S7_SIM) || 
			(this.driverTypeSauce.equals(DriverTypeSauce.GOOGLE_PIXEL_C_SIM)) ) { 
			dCaps = DesiredCapabilities.android();
		}
	}

	/**
	 * Your Sauce Labs username and access key in system variables.
	 */
	private final void authenticateSauceLabs() { 
		dCaps.setCapability("username", System.getenv("SAUCE_USERNAME"));
		dCaps.setCapability("accessKey", System.getenv("SAUCE_ACCESS_KEY"));		
	}


	/**
	 * This Sauce Labs test will try to run in this tunnel, so make 
	 * sure it's open first.
	 * <p>
	 * If this tunnel isn't open when your test begins, your test 
	 * will fail from the setup method in a recognizable way. Start 
	 * up your tunnel, and try again.
	 */
	private final void setTunnelSauceLabs() { 
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		System.out.println("\n" + methodNameLocal + ":\tyour tunnel is " + System.getenv("SAUCE_TUNNEL"));
		dCaps.setCapability("tunnelIdentifier", System.getenv("SAUCE_TUNNEL"));
	}

	/**
	 * 
	 * @param nameOfCallingTest - The test name will appear in your Dashboard.
	 */
	public void setNameOfCallingTest(String nameOfCallingTest) { 
		this.nameOfCallingTest = nameOfCallingTest;
	}
	public String getNameOfCallingTest() { 
		return this.nameOfCallingTest;
	}


	/**
	 * The next three values are defensive, and appropriate for most of our testing. 
	 * <p>
	 * These values can be overridden, but please be careful. And do it on your 
	 * own branch. 
	 * <p>
	 * Whatever these values are set to, they will apply to all tests running 
	 * in Sauce Labs.
	 * <p>
	 * See the JavaDoc for these values in SauceLabsConstants.java.
	 * <p>
	 * @see src/main/java/common/SauceLabsConstants.java
	 */
	private final void setSauceLabsGlobalTimeouts() { 
		dCaps.setCapability("commandTimeout", SauceLabsConstants.COMMAND_TIMEOUT);
		dCaps.setCapability("idleTimeout", SauceLabsConstants.IDLE_TIMEOUT);
		dCaps.setCapability("maxDuration", SauceLabsConstants.MAX_DURATION);		
	}

	/**
	 * Call this method to return a ready-to-use driver instance.
	 * <p>
	 * From within your test, do something like this:
	 * <p>
	 * <pre>
	 * driver = new CommonDriversSauce(DriverType.WINDOWS_CHROME, DataCenter.USA, "AllenTunnel").getPreparedDriver(method.getName());
	 * </pre>
	 * @param getNameOfCallingTest() - The test name will appear in your Dashboard.
	 * @return - An instance of RemoteWebDriver configured for immediate use in a test.
	 */
	public WebDriver getPreparedDriver() { 

		switch(this.driverTypeSauce) { 

		case IPHONE_6SP_SIM : { 
			dCaps.setCapability("appiumVersion", "1.13.0");
			dCaps.setCapability("deviceName","iPhone 6s Plus Simulator");
			dCaps.setCapability("deviceOrientation", "portrait");
			dCaps.setCapability("platformVersion","12.2");
			dCaps.setCapability("platformName", "iOS");
			dCaps.setCapability("browserName", "Safari");			
			
			/*
			 * Name of currently running test will appear in Sauce Labs Dashboard.
			 */
			dCaps.setCapability("name", getNameOfCallingTest());

			URL toDeviceCloud = null;
			try {
				if(this.dataCenter.equals(DataCenter.USA)) { 
					toDeviceCloud = new URL(SauceLabsConstants.URL_DEVICE_CLOUD_USA);
				}else if(this.dataCenter.equals(DataCenter.EU)) { 
					toDeviceCloud = new URL(SauceLabsConstants.URL_DEVICE_CLOUD_EU);
				}
			} catch (MalformedURLException mUE) {
				System.out.println(getNameOfCallingTest() + ":\tError creating new URL from input");
				throw new IllegalStateException(getNameOfCallingTest(), mUE);
			}

			driverToReturn = new RemoteWebDriver(toDeviceCloud,dCaps);

			break;
		} // end case
		
		case IPHONE_8_SIM : { 
			dCaps.setCapability("appiumVersion", "1.9.1");
			dCaps.setCapability("deviceName","iPhone 8 Simulator");
			dCaps.setCapability("deviceOrientation", "portrait");
			dCaps.setCapability("platformVersion","12.0");
			dCaps.setCapability("platformName", "iOS");
			dCaps.setCapability("browserName", "Safari");
			
			/*
			 * Name of currently running test will appear in Sauce Labs Dashboard.
			 */
			dCaps.setCapability("name", getNameOfCallingTest());

			URL toDeviceCloud = null;
			try {
				if(this.dataCenter.equals(DataCenter.USA)) { 
					toDeviceCloud = new URL(SauceLabsConstants.URL_DEVICE_CLOUD_USA);
				}else if(this.dataCenter.equals(DataCenter.EU)) { 
					toDeviceCloud = new URL(SauceLabsConstants.URL_DEVICE_CLOUD_EU);
				}
			} catch (MalformedURLException mUE) {
				System.out.println(getNameOfCallingTest() + ":\tError creating new URL from input");
				throw new IllegalStateException(getNameOfCallingTest(), mUE);
			}

			driverToReturn = new RemoteWebDriver(toDeviceCloud,dCaps);

			break;
		} // end case

		case IPHONE_X_SIM : { 
			dCaps.setCapability("appiumVersion", "1.12.1");
			dCaps.setCapability("deviceName","iPhone X Simulator");
			dCaps.setCapability("deviceOrientation", "portrait");
			dCaps.setCapability("platformVersion","12.2");
			dCaps.setCapability("platformName", "iOS");
			dCaps.setCapability("browserName", "Safari");	
			
			/*
			 * Name of currently running test will appear in Sauce Labs Dashboard.
			 */
			dCaps.setCapability("name", getNameOfCallingTest());

			URL toDeviceCloud = null;
			try {
				if(this.dataCenter.equals(DataCenter.USA)) { 
					toDeviceCloud = new URL(SauceLabsConstants.URL_DEVICE_CLOUD_USA);
				}else if(this.dataCenter.equals(DataCenter.EU)) { 
					toDeviceCloud = new URL(SauceLabsConstants.URL_DEVICE_CLOUD_EU);
				}
			} catch (MalformedURLException mUE) {
				System.out.println(getNameOfCallingTest() + ":\tError creating new URL from input");
				throw new IllegalStateException(getNameOfCallingTest(), mUE);
			}

			driverToReturn = new RemoteWebDriver(toDeviceCloud,dCaps);

			break;
		} // end case

		case SAMSUNG_GALAXY_S7_SIM : { 
			dCaps.setCapability("appiumVersion", "1.8.1");
			dCaps.setCapability("deviceName","Samsung Galaxy S7 HD GoogleAPI Emulator");
			dCaps.setCapability("deviceOrientation", "portrait");
			dCaps.setCapability("browserName", "Chrome");
			dCaps.setCapability("platformVersion", "8.0");
			dCaps.setCapability("platformName","Android");
			
			/*
			 * Name of currently running test will appear in Sauce Labs Dashboard.
			 */
			dCaps.setCapability("name", getNameOfCallingTest());

			URL toDeviceCloud = null;
			try {
				if(this.dataCenter.equals(DataCenter.USA)) { 
					toDeviceCloud = new URL(SauceLabsConstants.URL_DEVICE_CLOUD_USA);
				}else if(this.dataCenter.equals(DataCenter.EU)) { 
					toDeviceCloud = new URL(SauceLabsConstants.URL_DEVICE_CLOUD_EU);
				}
			} catch (MalformedURLException mUE) {
				System.out.println(getNameOfCallingTest() + ":\tError creating new URL from input");
				throw new IllegalStateException(getNameOfCallingTest(), mUE);
			}

			driverToReturn = new RemoteWebDriver(toDeviceCloud,dCaps);

			break;
		} // end case


		case GOOGLE_PIXEL_C_SIM : { 
			dCaps.setCapability("appiumVersion", "1.9.1");
			dCaps.setCapability("deviceName","Google Pixel C GoogleAPI Emulator");
			dCaps.setCapability("deviceOrientation", "portrait");
			dCaps.setCapability("browserName", "Chrome");
			dCaps.setCapability("platformVersion", "8.1");
			dCaps.setCapability("platformName","Android");
			
			/*
			 * Name of currently running test will appear in Sauce Labs Dashboard.
			 */
			dCaps.setCapability("name", getNameOfCallingTest());

			URL toDeviceCloud = null;
			try {
				if(this.dataCenter.equals(DataCenter.USA)) { 
					toDeviceCloud = new URL(SauceLabsConstants.URL_DEVICE_CLOUD_USA);
				}else if(this.dataCenter.equals(DataCenter.EU)) { 
					toDeviceCloud = new URL(SauceLabsConstants.URL_DEVICE_CLOUD_EU);
				}
			} catch (MalformedURLException mUE) {
				System.out.println(getNameOfCallingTest() + ":\tError creating new URL from input");
				throw new IllegalStateException(getNameOfCallingTest(), mUE);
			}

			driverToReturn = new RemoteWebDriver(toDeviceCloud,dCaps);

			break;
		} // end case


		default:
			break;		
		}
		return driverToReturn;
	}
}



