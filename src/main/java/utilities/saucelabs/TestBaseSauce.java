package utilities.saucelabs;

import org.openqa.selenium.WebDriver;

/**
 * Superclass, or test base, for functional front end tests with a Sauce Labs driver 
 * (run tests in the Sauce Labs cloud).
 * <p>
 * A separate superclass is available for front end tests with a local driver 
 * (run tests on your machine).
 * <p> 
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 * @see TestBaseSauceMobile.java
 * @see TestBaseSauce.java
 * @see TestBaseSauceMobile.java
 */
public class TestBaseSauce {

	/**
	 * The driver in the child test will be of this type.
	 */
	private DriverTypeSauce driverType = DriverTypeSauce.MAC_CHROME;

	/**
	 * Optional, for testability. To loop through supported browser types 
	 * when testing the utility itself. Or to simply override the currently 
	 * declared value above.
	 * <p>
	 * @param driverTypeToTest
	 */
	protected void setDriverTypeForTestClass(DriverTypeSauce driverTypeToTest) { 
		this.driverType = driverTypeToTest;
	}
	protected DriverTypeSauce getDriverTypeForTestClass() { 
		return this.driverType;
	}

	/**
	 * Set data center here (which Sauce Labs data center). 
	 * Nearly all the time, here at progressive, you'll want USA.
	 */
	private DataCenter dataCenter = DataCenter.USA;
	
	/**
	 * Instantiate without arguments.
	 */
	public TestBaseSauce() { 
		// no args constructor
	}
	
	public WebDriver getPreparedDriver(String testNameIn) { 
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();
		CommonDriverSauceDesktop cdsd = new CommonDriverSauceDesktop(driverType, dataCenter);
		cdsd.setNameOfCallingTest(testNameIn);
		System.out.println("\n" + methodNameLocal + " returns driver type " + this.driverType.toString() + " for " + testNameIn + "\n");
		return cdsd.getPreparedDriver();
	}
	
	public WebDriver getPreparedDriverMobile(String testNameIn) { 
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();
		CommonDriverSauceMobile cdsm = new CommonDriverSauceMobile(driverType, dataCenter);
		cdsm.setNameOfCallingTest(testNameIn);
		System.out.println("\n" + methodNameLocal + " returns driver type " + this.driverType.toString() + " for " + testNameIn + "\n");
		return cdsm.getPreparedDriver();
	}
	
	public WebDriver getPreparedDriverDevice(String testNameIn) { 
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();
		CommonDriverSauceDevice cdsd = new CommonDriverSauceDevice(driverType);
		cdsd.setNameOfCallingTest(testNameIn);
		System.out.println("\n" + methodNameLocal + " returns driver type " + this.driverType.toString() + " for " + testNameIn + "\n");
		return cdsd.getPreparedDriver();
	}
	

}
