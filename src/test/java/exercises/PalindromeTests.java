package exercises;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;

/**
 * This is the test class for src/main/java/Palindrome.
 * <p>
 * Good things to discuss about this one:
 * <ul>
 * <li>If this was customer-facing, would it change the tests your wrote for it?</li>
 * </ul>
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 * @see src.main.java.Palindrome.java
 */
public class PalindromeTests {

	private Palindrome classUnderTest;

	@BeforeMethod
	public void beforeMethod() {
		classUnderTest = new Palindrome();
	}

	@Test
	public void testHappyPathTrue() {
		String[] trueCases = {"civic","racecar","kayak"};
		boolean resultCaptured = true;
		for(String S : trueCases) { 
			if(!classUnderTest.isPalindrome(S)) { 
				resultCaptured = false;
			}
		}
		Assert.assertTrue(resultCaptured);
	}

	@Test
	public void testHappyPathFalse() {
		String[] trueCases = {"Dominique","bonjour","allegresse"};
		boolean resultCaptured = false;
		for(String S : trueCases) { 
			if(classUnderTest.isPalindrome(S)) { 
				resultCaptured = true;
			}
		}
		Assert.assertFalse(resultCaptured);
	}
	
	@Test(expectedExceptions=IllegalArgumentException.class)
	public void testInsufficientLength() { 
		String tooShort = "Do";
		classUnderTest.isPalindrome(tooShort);
	}

	@Test(expectedExceptions=IllegalArgumentException.class)
	public void testIllegalSpace() { 
		String containsSpace = "Bon jour";
		classUnderTest.isPalindrome(containsSpace);
	}

	@AfterMethod
	public void afterMethod() {
		classUnderTest = null;
	}

}
