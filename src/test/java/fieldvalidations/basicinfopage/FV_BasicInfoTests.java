package fieldvalidations.basicinfopage;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import common.ProgressiveLeasingConstants;

/**
 * Optional helper class for all field validation tests for basic info page.
 * <p>
 * Abtract out some common behavior; change environment in one place.
 * <p>
 * Version 1.0 2019-04-19
 * <p>
 * @author <a href="mailto:jay.loew@progleasing.com">Allen</a>
 * @version 1.0
 */
public class FV_BasicInfoTests {

	/**
	 * Tests in this package begin here.
	 * <p>
	 * Normal use case for our team is to switch between 12 and 13.
	 * <p>
	 * Here's how to do it without passing a parameter in from the XML (which 
	 * we're hoping to avoid).
	 * <p>
	 * @return Starting URL for all tests in this class.
	 */
	public static final String getStartingURL() { 
		return ProgressiveLeasingConstants.URL_BASIC_INFO_PAGE_12;
	}
	
	/**
	 * Tests in this package can safely wait during setup by calling this method.
	 * <p>
	 * Max wait can be changed of course, as can the field to wait for.
	 * <p>
	 * @param driver - Pass in the active driver instance.
	 */
	public static final void commonWaitInSetup(WebDriver driver) { 
		final String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();
		try { 
			new WebDriverWait(driver, 60).until(ExpectedConditions.presenceOfElementLocated(By.id("dob-month-input")));
		}catch(TimeoutException tE) { 
			throw new AssertionError(methodNameLocal, tE);
		}		
	}
}
