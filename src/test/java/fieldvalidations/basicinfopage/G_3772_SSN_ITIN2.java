package fieldvalidations.basicinfopage;

import java.lang.reflect.Method;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import pages.BasicInfoPage;
import utilities.saucelabs.TestBaseSauce;

/**
 * TestLink path:
 * <p>
 * General | Running | ECOM_UI | ECOM_FieldValidations | G-3772:Field validation rules: Basic Info: SSN / ITIN
 * <p>
 * Sample direct nav URL:
 * <p>
 * https://vdc-qaswebapp12.stormwind.local/eComUI/#/apply/basic-info
 * <p>
 * Run from: src/test/resources/TestNGSuiteFiles/FieldValidations/FieldValidations.xml
 * <p>
 * Version 1.0 2019-01-29
 * <p>
 * Version 1.1 2019-01-30
 * <p>
 * Version 1.2 2019-02-04 add three new happy path cases
 * <p>
 * Version 1.3 2019-03-01 performance enhancements and re-test
 * <p>
 * Version 1.4 2019-04-19 abstract out common behavior and tune for performance
 * <p>
 * @author <a href="mailto:jay.loew@progleasing.com">Allen</a>
 * @author <a href="mailto:christopher.rains@progleasing.com">CJ</a>
 * @version 1.4
 */
public class G_3772_SSN_ITIN2 extends TestBaseSauce {

	private WebDriver driver;
	
	/**
	 * Happy-path placeholders in SSN / ITIN field: 888-88-8888.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testSSNHappyPlaceholders(Method method) {

		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);
		
		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);

		pageUnderTest.getFieldNameFirst().click();

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		FieldValidationUtilities.tabOut(testNameLocal, driver);

		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;
		
		pageUnderTest.getFieldSSN1_area().click();

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		StringBuilder placeholderCaptured = new StringBuilder(pageUnderTest.getFieldSSN1_area().getAttribute("placeholder"));

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		try { 
			Assert.assertEquals(placeholderCaptured.toString(), "888");
			System.out.println("\n" + testNameLocal + ":\t888 placeholder expected and found"); // first three
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL 888 placeholder: expected \'888\', captured \'" + placeholderCaptured.toString() + "\'");
			throw aE;
		}finally { 
			placeholderCaptured.delete(0, placeholderCaptured.length());
		}

		pageUnderTest.getFieldSSN1_area().click();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		placeholderCaptured.append(pageUnderTest.getFieldSSN2_group().getAttribute("placeholder"));

		try { 
			Assert.assertEquals(placeholderCaptured.toString(), "88");
			System.out.println("\n" + testNameLocal + ":\t88 placeholder expected and found"); // middle two
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL 88 placeholder: expected \'88\', captured \'" + placeholderCaptured.toString() + "\'");
			throw aE;
		}finally { 
			placeholderCaptured.delete(0, placeholderCaptured.length());
		}

		pageUnderTest.getFieldSSN3_serial().click();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		placeholderCaptured.append(pageUnderTest.getFieldSSN3_serial().getAttribute("placeholder"));

		try { 
			Assert.assertEquals(placeholderCaptured.toString(), "8888");
			System.out.println("\n" + testNameLocal + ":\t8888 placeholder expected and found"); // last four
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL 8888 placeholder: expected \'8888\', captured \'" + placeholderCaptured.toString() + "\'");
			throw aE;
		}finally { 
			placeholderCaptured.delete(0, placeholderCaptured.length());
			placeholderCaptured = null;		
			FieldValidationUtilities.refreshBetweenTest(testNameLocal,driver);
			testNameLocal = null;
			pageUnderTest = null;
		}
	}



	/**
	 * If subclass of TestBaseSauce, pass the test result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit driver.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		SessionId session = ((RemoteWebDriver)driver).getSessionId();
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) { 
			System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
			// could be null in a happy path run
			if(!(null==session)) { // don't use .equals() here
				((JavascriptExecutor) driver).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
			}
		} else { 
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		if(!(null==driver)) { 
			driver.quit();
		}
		System.out.println(methodNameLocal + ":\tEND tearDown.");
	}

}
