package fieldvalidations.basicinfopage;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import pages.BasicInfoPage;
import utilities.saucelabs.TestBaseSauce;

/**
 * TestLink path:
 * <p>
 * General | Running | ECOM_UI | ECOM_FieldValidations | G-3823:Field validation rules: Basic Info: Issuing State
 * <p>
 * Sample direct nav URL:
 * <p>
 * https://vdc-qaswebapp12.stormwind.local/eComUI/#/apply/basic-info
 * <p>
 * Run from: src/test/resources/TestNGSuiteFiles/FieldValidations/FieldValidations.xml
 * <p>
 * Version 1.0 2019-02-07
 * <p>
 * Version 1.2 2019-04-19 abstract out common behavior and tune for performance
 * <p>
 * @author <a href="mailto:jay.loew@progleasing.com">Allen</a>
 * @author <a href="mailto:shawn.hartke@progleasing.com">Shawn</a>
 * @version 1.1
 * @see G_3823_State1.java
 * @see G_3823_State2.java
 */
public class G_3823_State3 extends TestBaseSauce {

	private WebDriver driver;


	/**
	 * Leave state unselected and click out; verify expected error. 
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testNullState(Method method) {

		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);
		
		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);

		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;

		pageUnderTest.getFieldDate_YYYY().click();
		Actions builder = new Actions(driver);
		builder.sendKeys(Keys.TAB).build().perform();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		builder.sendKeys(Keys.TAB).build().perform();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		builder.sendKeys(Keys.TAB).build().perform();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		builder.sendKeys(Keys.TAB).build().perform();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		builder.sendKeys(Keys.TAB).build().perform();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		builder.sendKeys(Keys.TAB).build().perform();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		builder.sendKeys(Keys.TAB).build().perform();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		builder.sendKeys(Keys.TAB).build().perform();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;

		String expected = "Your issuing state is required to process the application.";
		String captured = null;
		WebElement temp = null;

		try { 
			temp = driver.findElement(By.xpath("//pg-issuing-state/div/pg-field/div[2]/span"));
			// //pg-issuing-state[@id='issuing-state']/div/pg-field/div[2]/span
		}catch(NoSuchElementException nSEE) { 
			throw new AssertionError(testNameLocal, nSEE);
		}

		captured = temp.getText().trim();

		try { 
			Assert.assertEquals(captured, expected);
			System.out.println("\n" + testNameLocal + ": PASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ": FAIL");
			System.out.println(testNameLocal + ": expected \'" + expected + "\'");
			System.out.println(testNameLocal + ": captured \'" + captured + "\'");
			throw aE;
		}finally { 
			expected = null;
			captured = null;
			temp = null;
			pageUnderTest = null;
			testNameLocal = null;
			builder = null;
		}	
	}	



	/**
	 * If subclass of TestBaseSauce, pass the test result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit driver.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		SessionId session = ((RemoteWebDriver)driver).getSessionId();
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) { 
			System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
			// could be null in a happy path run
			if(!(null==session)) { // don't use .equals() here
				((JavascriptExecutor) driver).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
			}
		} else { 
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		if(!(null==driver)) { 
			driver.quit();
		}
		System.out.println(methodNameLocal + ":\tEND tearDown.");
	}

}
