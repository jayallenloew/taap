package fieldvalidations.basicinfopage;

import java.lang.reflect.Method;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import pages.BasicInfoPage;
import utilities.saucelabs.TestBaseSauce;

/**
 * TestLink path:
 * <p>
 * General | Running | ECOM_UI | ECOM_FieldValidations | G-3861:Field validation rules: Basic Info: First and Last Name fields
 * <p>
 * Sample direct nav URL:
 * <p>
 * https://vdc-qaswebapp12.stormwind.local/eComUI/#/apply/basic-info
 * <p>
 * Run from: src/test/resources/TestNGSuiteFiles/FieldValidations/FieldValidations.xml
 * <p>
 * Version 1.1 2019-01-24
 * <p>
 * Version 1.2 2019-02-27 minor fixes
 * <p>
 * Version 1.3 2019-04-19
 * <p>
 * Version 1.4 2019-04-19 abstract out common behavior and tune for performance
 * <p>
 * @author <a href="mailto:jay.loew@progleasing.com">Allen Loew</a>
 * @author <a href="mailto:christopher.rains@progleasing.com">CJ Rains</a>
 * @version 1.4
 */
public class G_3861_Names extends TestBaseSauce {

	private WebDriver driver;
	
	public static final char[] specialCharactersNameFields = { 
			'1', '2', '3', '4', '5', '6', '7', '8', '9', '*', '+', '/' 
	};



	/**
	 * Blank values in first name and last name fields.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true,priority=1)
	public void testNamesBlank(Method method) {
		
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);

		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		pageUnderTest.getFieldNameFirst().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldNameFirst().click();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		// click into last name field having left first name field blank
		pageUnderTest.getFieldNameLast().click();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);		
		StringBuilder expected = new StringBuilder("Your first name is required to process the application.");
		// go back to last and verify alert icon and text
		WebElement icon = driver.findElement(By.cssSelector("span.pg-field-error"));
		Assert.assertTrue(icon.isDisplayed());
		Assert.assertTrue(expected.toString().equals(icon.getText()));
		pageUnderTest.getFieldNameFirst().sendKeys("Bo");
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		Assert.assertFalse(icon.isDisplayed());
		expected.delete(0, expected.length());
		icon = null;
		
		expected = new StringBuilder("Your last name is required to process the application.");
		pageUnderTest.getFieldNameFirst().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldNameLast().click();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldNameLast().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldNameFirst().clear();
		icon = driver.findElement(By.xpath("//pg-last-name[@id='last-name']/div/pg-field/div[2]/span"));
		// .is-hovered .pg-field-error:nth-child(1)+
		Assert.assertTrue(icon.isDisplayed());
		Assert.assertTrue(expected.toString().equals(icon.getText()));
		pageUnderTest.getFieldNameLast().sendKeys("Bo");
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		Assert.assertFalse(icon.isDisplayed());
		System.out.println("\n" + testNameLocal + ":\tPASS");
		expected.delete(0, expected.length());
		expected = null;
		icon = null;
		testNameLocal = null;
		pageUnderTest = null;
	}


	/**
	 * A special character in position 0. First name.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true,priority=2)
	public void testNameFirstBeginSpecialCharacter(Method method) {
		
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);

		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);

		String nameUnderTest = "-Sivasubramanian"; // illegal: special character at position zero of first name

		pageUnderTest.getFieldNameFirst().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		pageUnderTest.getFieldNameFirst().sendKeys(nameUnderTest); 
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		// must click out somewhere
		pageUnderTest.getFieldNameLast().click();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		String expected = "Your first name must start with a letter.";
		String captured = driver.findElement(By.xpath("//pg-first-name[@id='first-name']/div/pg-field/div[2]/span[4]")).getText();

		try { 
			Assert.assertTrue(captured.equals(expected.toString()));
			System.out.println("\n" + testNameLocal + ": PASS with \'" + nameUnderTest + "\'");
		} catch(AssertionError aE) {
			System.out.println("\n" + testNameLocal + ":\tFAIL with \'" + nameUnderTest + "\'");
			System.out.println(testNameLocal + ":\texpected \'" + expected + "\'");
			System.out.println(testNameLocal + ":\tcaptured \'" + captured + "\'");
			throw aE;
		}finally { 
			expected = null;
			captured = null;
			nameUnderTest = null;
			testNameLocal = null;
			pageUnderTest = null;
		}
	}	



	/**
	 * A special character in any position. First name.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true,priority=3)
	public void testNameFirstContainsSpecChar(Method method) {
		
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);

		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);
		
		// begin with a normal name
		StringBuilder nameUnderTest = new StringBuilder("Amanda");
		
		// into any position thereof (but not position 0), insert one of the special characters
		int offsetInt = new java.util.Random().nextInt(nameUnderTest.length());
		if(offsetInt==0) { 
			offsetInt++; // don't re-test position 0 here
		}
		
		char toInsert = specialCharactersNameFields[new java.util.Random().nextInt(specialCharactersNameFields.length)];
		nameUnderTest.insert(offsetInt, toInsert);
				
		pageUnderTest.getFieldNameFirst().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldNameFirst().sendKeys(nameUnderTest); // illegal: special character at any non-zero position
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		// must click out somewhere
		pageUnderTest.getFieldNameLast().click();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		StringBuilder expected = new StringBuilder("First name can't contain any numbers or special characters other than dash (-), apostrophe ('), or a space.");
		String captured = driver.findElement(By.xpath("//pg-first-name[@id='first-name']/div/pg-field/div[2]/span[2]")).getText();

		try { 
			Assert.assertTrue(captured.equals(expected.toString()));
			System.out.println("\n" + testNameLocal + ": PASS with \'" + nameUnderTest + "\'");
		} catch(AssertionError aE) {
			System.out.println("\n" + testNameLocal + ":\tFAIL with \'" + nameUnderTest + "\'");
			System.out.println(testNameLocal + ":\texpected \'" + expected + "\'");
			System.out.println(testNameLocal + ":\tcaptured \'" + captured + "\'");
			throw aE;
		}finally { 
			expected.delete(0, expected.length());
			expected = null;
			nameUnderTest.delete(0, nameUnderTest.length());
			nameUnderTest = null;
			captured = null;
			testNameLocal = null;
			pageUnderTest = null;
		}
	}	
	



	/**
	 * A special character in any position. Last name.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true,priority=4)
	public void testNameLastContainsSpecChar(Method method) {
		
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);

		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);

		// begin with a normal name
		StringBuilder nameUnderTest = new StringBuilder("Robertson");
		
		// into any position thereof (but not position 0), insert one of the special characters
		int offsetInt = new java.util.Random().nextInt(nameUnderTest.length());
		if(offsetInt==0) { 
			offsetInt++; // don't re-test position 0 here
		}
		
		char toInsert = specialCharactersNameFields[new java.util.Random().nextInt(specialCharactersNameFields.length)];
		nameUnderTest.insert(offsetInt, toInsert);
				
		pageUnderTest.getFieldNameLast().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldNameLast().sendKeys(nameUnderTest); // illegal: special character at any non-zero position
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		// must click out somewhere
		pageUnderTest.getFieldNameFirst().click();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		StringBuilder expected = new StringBuilder("Your last name can't contain any numbers or special characters other than dash (-), apostrophe ('), or a space.");
		String captured = driver.findElement(By.xpath("//pg-last-name[@id='last-name']/div/pg-field/div[2]/span[2]")).getText();

		try { 
			Assert.assertTrue(captured.equals(expected.toString()));
			System.out.println("\n" + testNameLocal + ": PASS with \'" + nameUnderTest + "\'");
		} catch(AssertionError aE) {
			System.out.println("\n" + testNameLocal + ":\tFAIL with \'" + nameUnderTest + "\'");
			System.out.println(testNameLocal + ":\texpected \'" + expected + "\'");
			System.out.println(testNameLocal + ":\tcaptured \'" + captured + "\'");
			throw aE;
		}finally { 
			expected.delete(0, expected.length());
			expected = null;
			nameUnderTest.delete(0, nameUnderTest.length());
			nameUnderTest = null;
			captured = null;
			testNameLocal = null;
			pageUnderTest = null;
		}
	}	
	
	
	
	/**
	 * A special character in position 0. Last name.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true,priority=5)
	public void testNameLastBeginSpecialCharacter(Method method) {
		
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);

		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);

		String nameUnderTest = "-Mary-Jo"; // illegal: special character at position zero of last name
		
		pageUnderTest.getFieldNameLast().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldNameLast().sendKeys(nameUnderTest); 
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		// must click out somewhere
		pageUnderTest.getFieldNameFirst().click();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		StringBuilder expected = new StringBuilder("Your last name must start with a letter.");
		String captured = driver.findElement(By.xpath("//pg-last-name/div/pg-field/div[2]/span[4]")).getText();

		try { 
			Assert.assertTrue(captured.equals(expected.toString()));
			System.out.println("\n" + testNameLocal + ": PASS with \'" + nameUnderTest + "\'");
		} catch(AssertionError aE) {
			System.out.println("\n" + testNameLocal + ":\tFAIL with \'" + nameUnderTest + "\'");
			System.out.println(testNameLocal + ":\texpected \'" + expected + "\'");
			System.out.println(testNameLocal + ":\tcaptured \'" + captured + "\'");
			throw aE;
		}finally { 
			expected.delete(0, expected.length());
			expected = null;
			captured = null;
			nameUnderTest = null;
			testNameLocal = null;
			pageUnderTest = null;
		}
	}	
	
	
	/**
	 * Silent truncation after position 40. Boundary testing.
	 * <p>
	 * In other words, >40 would be legal, but 41 and following are ignored.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true,priority=6)
	public void testTruncationNameFirst(Method method) {
		
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);

		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);

		HashMap<Integer,String> longNames = new HashMap<Integer,String>();
		// phony-baloney pseudo-French long names at and past outer boundary
		longNames.put(40, "Marie-HeleneSocratte SeverineFlauberston");		// 40
		longNames.put(41, "Marie-HeleneSocratte SeverineFlauberstons");		// 41
		longNames.put(43, "Marie-HeleneSocratte SeverineFlauberstonsau");	// 43
		
		pageUnderTest.getFieldNameFirst().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldNameFirst().sendKeys(longNames.get(40)); 
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		StringBuilder captured = new StringBuilder(pageUnderTest.getFieldNameFirst().getAttribute("value"));
		
		try { 
			Assert.assertEquals(captured.toString(), longNames.get(40));
			System.out.println("\n" + testNameLocal + ": PASS at boundary");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ": FAIL at boundary");
			System.out.println(testNameLocal + ": expected \'" + longNames.get(40) + "\'");
			System.out.println(testNameLocal + ": captured \'" + captured.toString() + "\'");
			throw aE;
		}finally { 
			captured.delete(0, captured.length());
			captured = null;
		}	
		
		pageUnderTest.getFieldNameFirst().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldNameFirst().sendKeys(longNames.get(41)); 
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		captured = new StringBuilder(pageUnderTest.getFieldNameFirst().getAttribute("value"));
		
		try { 
			Assert.assertEquals(captured.toString(), longNames.get(40));
			System.out.println("\n" + testNameLocal + ": PASS at boundary plus one");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ": FAIL at boundary plus one");
			System.out.println(testNameLocal + ": expected \'" + longNames.get(40) + "\'");
			System.out.println(testNameLocal + ": captured \'" + captured.toString() + "\'");
			throw aE;
		}finally { 
			captured.delete(0, captured.length());
			captured = null;
		}
		
		pageUnderTest.getFieldNameFirst().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldNameFirst().sendKeys(longNames.get(43)); 
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		captured = new StringBuilder(pageUnderTest.getFieldNameFirst().getAttribute("value"));
		
		try { 
			Assert.assertEquals(captured.toString(), longNames.get(40));
			System.out.println("\n" + testNameLocal + ": PASS at boundary plus three");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ": FAIL at boundary plus three");
			System.out.println(testNameLocal + ": expected \'" + longNames.get(40) + "\'");
			System.out.println(testNameLocal + ": captured \'" + captured.toString() + "\'");
			throw aE;
		}finally { 
			captured.delete(0, captured.length());
			captured = null;
			longNames = null;
			testNameLocal = null;
			pageUnderTest = null;
		}
	}	
	
	
	/**
	 * Silent truncation after position 40. Boundary testing.
	 * <p>
	 * In other words, >40 would be legal, but 41 and following are ignored.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true,priority=7)
	public void testTruncationNameLast(Method method) {
		
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_BasicInfoTests.getStartingURL());
		
		FV_BasicInfoTests.commonWaitInSetup(driver);

		BasicInfoPage pageUnderTest = new BasicInfoPage(driver);

		HashMap<Integer,String> longNames = new HashMap<Integer,String>();
		// phony-balony pseudo-French long names at and past outer boundary
		longNames.put(40, "Marie-HeleneSocratte SeverineFlauberston");		// 40
		longNames.put(41, "Marie-HeleneSocratte SeverineFlauberstons");		// 41
		longNames.put(43, "Marie-HeleneSocratte SeverineFlauberstonsau");	// 43
		
		pageUnderTest.getFieldNameLast().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldNameLast().sendKeys(longNames.get(40)); 
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		StringBuilder captured = new StringBuilder(pageUnderTest.getFieldNameLast().getAttribute("value"));
		
		try { 
			Assert.assertEquals(captured.toString(), longNames.get(40));
			System.out.println("\n" + testNameLocal + ": PASS at boundary");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ": FAIL at boundary");
			System.out.println(testNameLocal + ": expected \'" + longNames.get(40) + "\'");
			System.out.println(testNameLocal + ": captured \'" + captured.toString() + "\'");
			throw aE;
		}finally { 
			captured.delete(0, captured.length());
			captured = null;
		}	
		
		pageUnderTest.getFieldNameLast().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldNameLast().sendKeys(longNames.get(41)); 
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		captured = new StringBuilder(pageUnderTest.getFieldNameLast().getAttribute("value"));
		
		try { 
			Assert.assertEquals(captured.toString(), longNames.get(40));
			System.out.println("\n" + testNameLocal + ": PASS at boundary plus one");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ": FAIL at boundary plus one");
			System.out.println(testNameLocal + ": expected \'" + longNames.get(40) + "\'");
			System.out.println(testNameLocal + ": captured \'" + captured.toString() + "\'");
			throw aE;
		}finally { 
			captured.delete(0, captured.length());
			captured = null;
		}
		
		pageUnderTest.getFieldNameLast().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldNameLast().sendKeys(longNames.get(43)); 
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		
		captured = new StringBuilder(pageUnderTest.getFieldNameLast().getAttribute("value"));
		
		try { 
			Assert.assertEquals(captured.toString(), longNames.get(40));
			System.out.println("\n" + testNameLocal + ": PASS at boundary plus three");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ": FAIL at boundary plus three");
			System.out.println(testNameLocal + ": expected \'" + longNames.get(40) + "\'");
			System.out.println(testNameLocal + ": captured \'" + captured.toString() + "\'");
			throw aE;
		}finally { 
			captured.delete(0, captured.length());
			captured = null;
			longNames = null;
			testNameLocal = null;
			pageUnderTest = null;
		}
	}		
	
	
	/**
	 * If subclass of TestBaseSauce, pass the test result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit driver.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		SessionId session = ((RemoteWebDriver)driver).getSessionId();
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) { 
			System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
			// could be null in a happy path run
			if(!(null==session)) { // don't use .equals() here
				((JavascriptExecutor) driver).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
			}
		} else { 
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		if(!(null==driver)) { 
			driver.quit();
		}
		System.out.println(methodNameLocal + ":\tEND tearDown.");
	}

}
