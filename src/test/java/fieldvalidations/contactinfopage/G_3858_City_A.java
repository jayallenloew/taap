package fieldvalidations.contactinfopage;

import java.lang.reflect.Method;
import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import pages.ContactInfoPage;
import utilities.saucelabs.DriverTypeSauce;
import utilities.saucelabs.TestBaseSauce;


/**
 * Field validation test: Contact Info page: city values.
 * <p>
 * In this test class: positive scenarios, static setup.
 * <p>
 * Full coverage for the TestLink case below:
 * <p>
 * http://slc-qaswebtli01/testlink/linkto.php?tprojectPrefix=G&item=testcase&id=G-3858
 * <p>
 * Version 1.0 2019-02-28
 * <p>
 * Version 2.0 2019-04-03:
 * <ul>
 * <li>Remove deprecated setup code.</li>
 * <li>Default to Sauce drivers.</li>
 * <li>Update changed elements.</li>
 * <li>Enhance performance.</li>
 * <li>Re-test.</li>
 * </ul>
 * Version 2.1 2019-04-19 abstract out common behavior and tune for performance
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 2.1
 * @see ContactInfoPage
 * @see G_3858_City_B
 * @see G_3858_City_C
 */
public class G_3858_City_A extends TestBaseSauce {

	private WebDriver driver;

	private ContactInfoPage pageUnderTest;
	
	private static HashMap<Integer,String> variousNames;
	
	
	
	@BeforeClass
	public static void setUpStatic() {
		variousNames = new HashMap<Integer,String>();
		variousNames.put(37, "Velizy Villacoublay.Rotondre-Phillipe");			// 37
		variousNames.put(40, "Velizy Villacoublay.Rotondre-Phillipe Au");		// 40
		variousNames.put(45, "Velizy Villacoublay.Rotondre-Phillipe Auguste");	// 45
	}

	
	/**
	 * Ensure that city name can contain a period. Some city names 
	 * contain a period, i.e. St. Paul, and must not throw an error.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testAllowedPeriod(Method method) {

		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		
		super.setDriverTypeForTestClass(DriverTypeSauce.WINDOWS_CHROME);
		
		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_ContactInfoTests.getStartingURL());
		
		FV_ContactInfoTests.commonWaitInSetup(driver);
		
		pageUnderTest = new ContactInfoPage(driver);

		String cityUnderTest = "St. Paul"; // contains period and it's legal

		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;

		WebElement fieldTemp = pageUnderTest.getFieldCity();

		try { 
			fieldTemp.sendKeys(cityUnderTest);
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(testNameLocal, anyExceptionWhatsoever);
		}finally { 
			fieldTemp = null;
		}

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldZIP().click();

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		try { 
			Assert.assertTrue(fetchSuccessCheckmark(testNameLocal).isDisplayed()); 
			System.out.println("\n" + testNameLocal + ":\tPASS with \'" + cityUnderTest + "\'");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL with \'" + cityUnderTest + "\'; expected checkmark missing");
			throw aE;
		}finally { 
			FieldValidationUtilities.refreshBetweenTest(testNameLocal, driver);
			fieldTemp = null;
			cityUnderTest = null;
			testNameLocal = null;
			pageUnderTest = null;
		}		
	}


	/**
	 * Ensure that city name can contain a space. Two-word city names 
	 * are common, i.e. San Francisco, and must not throw an error.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testAllowedSpace(Method method) {

		String testNameLocal = getClass().getSimpleName() + "." + method.getName();

		super.setDriverTypeForTestClass(DriverTypeSauce.WINDOWS_FIREFOX);

		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_ContactInfoTests.getStartingURL());
		
		FV_ContactInfoTests.commonWaitInSetup(driver);
		
		pageUnderTest = new ContactInfoPage(driver);

		String cityUnderTest = "Santa Clara"; // contains space and it's legal

		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;
		
		WebElement fieldTemp = pageUnderTest.getFieldCity();

		try { 
			fieldTemp.sendKeys(cityUnderTest);
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(testNameLocal, anyExceptionWhatsoever);
		}finally { 
			fieldTemp = null;
		}

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldZIP().click();

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		try { 
			Assert.assertTrue(fetchSuccessCheckmark(testNameLocal).isDisplayed()); 
			System.out.println("\n" + testNameLocal + ":\t\tPASS with \'" + cityUnderTest + "\'");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\t\tFAIL with \'" + cityUnderTest + "\'; expected checkmark missing");
			throw aE;
		}finally { 
			FieldValidationUtilities.refreshBetweenTest(testNameLocal, driver);
			fieldTemp = null;
			cityUnderTest = null;
			testNameLocal = null;
			pageUnderTest = null;
		}
	}


	/**
	 * Ensure that city name can contain a hyphen. Some city names 
	 * contain a hyphen, i.e. Vélizy-Villacoublay, and must not 
	 * throw an error.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testAllowedHyphen(Method method) {

		String testNameLocal = getClass().getSimpleName() + "." + method.getName();

		super.setDriverTypeForTestClass(DriverTypeSauce.WINDOWS_IE);

		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_ContactInfoTests.getStartingURL());
		
		FV_ContactInfoTests.commonWaitInSetup(driver);
				
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;
		
		pageUnderTest = new ContactInfoPage(driver);

		String cityUnderTest = "Vélizy-Villacoublay"; // contains hyphen and it's legal
		
		WebElement fieldTemp = pageUnderTest.getFieldCity();

		try { 
			fieldTemp.sendKeys(cityUnderTest);
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(testNameLocal, anyExceptionWhatsoever);
		}finally { 
			fieldTemp = null;
		}

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldZIP().click();

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		try { 
			Assert.assertTrue(fetchSuccessCheckmark(testNameLocal).isDisplayed()); 
			System.out.println("\n" + testNameLocal + ":\t" + "PASS with \'" + cityUnderTest + "\'");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\t" + "FAIL with \'" + cityUnderTest + "\'; the expected success check mark is missing.");
			throw aE;
		}finally { 
			FieldValidationUtilities.refreshBetweenTest(testNameLocal, driver);
			fieldTemp = null;
			cityUnderTest = null;
			testNameLocal = null;
			pageUnderTest = null;
		}
	}
	


	/**
	 * Validate length boundary, and silent truncation, in city name.
	 * <p>
	 * Boundary - 3
	 * @param method
	 */
	@Test(enabled=true)
	public void testTruncation1of3(Method method) {

		String testNameLocal = getClass().getSimpleName() + "." + method.getName();

		super.setDriverTypeForTestClass(DriverTypeSauce.MAC_CHROME);
		
		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_ContactInfoTests.getStartingURL());
		
		FV_ContactInfoTests.commonWaitInSetup(driver);
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;

		pageUnderTest = new ContactInfoPage(driver);
		
		WebElement fieldTemp = pageUnderTest.getFieldCity();

		try { 
			fieldTemp.sendKeys(variousNames.get(37));
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(testNameLocal, anyExceptionWhatsoever);
		}

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldZIP().click();

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		try { 
			Assert.assertTrue(fetchSuccessCheckmark(testNameLocal).isDisplayed()); 
			System.out.println("\n" + testNameLocal + ":\tPASS at boundary - 3");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL at boundary - 3; the expected success check mark is missing.");
			throw aE;
		}finally { 
			FieldValidationUtilities.refreshBetweenTest(testNameLocal, driver);
			fieldTemp = null;
			testNameLocal = null;
			pageUnderTest = null;
		}
	}	
	


	/**
	 * Validate length boundary, and silent truncation, in city name.
	 * <p>
	 * Boundary
	 * @param method
	 */
	@Test(enabled=true)
	public void testTruncation2of3(Method method) {

		String testNameLocal = getClass().getSimpleName() + "." + method.getName();

		super.setDriverTypeForTestClass(DriverTypeSauce.MAC_SAFARI);

		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_ContactInfoTests.getStartingURL());
		
		FV_ContactInfoTests.commonWaitInSetup(driver);
		
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;

		pageUnderTest = new ContactInfoPage(driver);

		WebElement fieldTemp = pageUnderTest.getFieldCity();

		try { 
			fieldTemp.sendKeys(variousNames.get(40));
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(testNameLocal, anyExceptionWhatsoever);
		}

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldZIP().click();

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		try { 
			Assert.assertTrue(fetchSuccessCheckmark(testNameLocal).isDisplayed()); 
			System.out.println("\n" + testNameLocal + ":\tPASS at boundary");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL at boundary");
			throw aE;
		}finally { 
			FieldValidationUtilities.refreshBetweenTest(testNameLocal, driver);
			fieldTemp = null;
			testNameLocal = null;
			pageUnderTest = null;
		}
	}	
	
	
	/**
	 * Validate length boundary, and silent truncation, in city name.
	 * <p>
	 * Boundary + 5
	 * @param method
	 */
	@Test(enabled=true)
	public void testTruncation3of3(Method method) {

		String testNameLocal = getClass().getSimpleName() + "." + method.getName();

		super.setDriverTypeForTestClass(DriverTypeSauce.MAC_FIREFOX);

		driver = getPreparedDriver(testNameLocal);
		
		driver.navigate().to(FV_ContactInfoTests.getStartingURL());
		
		FV_ContactInfoTests.commonWaitInSetup(driver);
		
		pageUnderTest = new ContactInfoPage(driver);

		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;
		
		WebElement fieldTemp = pageUnderTest.getFieldCity();

		try { 
			fieldTemp.sendKeys(variousNames.get(40));
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError(testNameLocal, anyExceptionWhatsoever);
		}

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldZIP().click();

		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		try { 
			Assert.assertTrue(fetchSuccessCheckmark(testNameLocal).isDisplayed()); 
			System.out.println("\n" + testNameLocal + ":\tPASS at boundary + 5");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAILat boundary + 5");
			throw aE;
		}finally { 
			FieldValidationUtilities.refreshBetweenTest(testNameLocal, driver);
			fieldTemp = null;
			testNameLocal = null;
			pageUnderTest = null;
		}
	}		
	


	
	/**
	 * Fetch the green "success" checkmark to assert on during happy-path tests.
	 * <p>
	 * @param testNameIn
	 * @return - An instance of org.openqa.selenium.WebElement
	 */
	private WebElement fetchSuccessCheckmark(String testNameIn) { 
		WebElement checkmarkTemp = null;
		try { 
			checkmarkTemp = driver.findElement(By.cssSelector(".city-wrapper > .pg-address-success-icon"));
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError((getClass().getSimpleName() + ":\t" + testNameIn + ":\tcouldn't create WebElement for the success checkmark"), anyExceptionWhatsoever);
		}		
		return checkmarkTemp;
	}


	/**
	 * If subclass of TestBaseSauce, pass the test result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit driver.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		SessionId session = ((RemoteWebDriver)driver).getSessionId();
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) { 
			System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
			// could be null in a happy path run
			if(!(null==session)) { // don't use .equals() here
				((JavascriptExecutor) driver).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
			}
		} else { 
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		if(!(null==driver)) { 
			driver.quit();
		}
		pageUnderTest = null;
		System.out.println(methodNameLocal + ":\tEND tearDown.");
	}
	
	@AfterClass
	public static void tearDownStatic() { 
		variousNames = null;
	}

}
