package fieldvalidations.contactinfopage;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import pages.ContactInfoPage;
import utilities.saucelabs.DriverTypeSauce;
import utilities.saucelabs.TestBaseSauce;
import waiter.Waiter;

/**
 * Field validation test: Contact Info page: email values.
 * <p>
 * Positive and negative cases.
 * <p>
 * Full coverage for the TestLink case below:
 * <p>
 * http://slc-qaswebtli01/testlink/linkto.php?tprojectPrefix=G&item=testcase&id=G-4115
 * <p>
 * Version 1.0 2019-01-23
 * <p>
 * Version 1.1 2019-04-19 abstract out common behavior and tune for performance
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.1
 * @see ContactInfoPage
 */
public class G_4115_Email_B extends TestBaseSauce {

	private WebDriver driver;

	/**
	 * Expected text after a valid email address is entered, or 
	 * after a simple click into the email address field.
	 * <p>
	 * @return Expected text of a field label.
	 */
	public static final String getHintEmailHappy() { 
		return "We'll send you updates on your application at this email address.";
	}

	/**
	 * Expected text after an illegal email address is entered: missing period.
	 * <p>
	 * @return Expected text of a field label.
	 */
	public static final String getHintEmailMissingPeriod() { 
		return "Your email address is missing a period ( . ).";
	}

	/**
	 * Expected text after an illegal email address is entered: contains space.
	 * <p>
	 * @return Expected text of a field label.
	 */
	public static final String getHintEmailContainsSpace() { 
		return "Your email address can't contain any spaces.";
	}


	/**
	 * Expected text after an illegal email address is entered: invalid characters.
	 * <p>
	 * @return Expected text of a field label.
	 */
	public static final String getHintEmailContainsInvalidCharacters() { 
		return "Your email address can't contain special characters other than at ( @ ), period ( . ), dash ( - ), underscore ( _ ) or plus ( + ).";
	}

	/**
	 * Expected text after an illegal email address is entered: incomplete .com.
	 * <p>
	 * @return Expected text of a field label.
	 */
	public static final String getHintEmailContainsIncompleteDotCom() { 
		return "Your email address is invalid. Please ensure it contains at least two letters after the period ( . ) symbol.";
	}



	/**
	 * Test the hint text for the email field as expected 
	 * after a normal, properly formatted (but not necessarily 
	 * real) email address is entered into the field.
	 * <p>
	 * See testEmailFieldHint1().
	 */
	@Test(enabled=true)
	public void testEmailHintHappy(Method method) {
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		setDriverTypeForTestClass(DriverTypeSauce.MAC_SAFARI);
		driver = getPreparedDriver(testNameLocal); 
		Waiter waiter = new Waiter();
		waiter.get(FV_ContactInfoTests.getStartingURL(), driver, 60);
		StringBuilder sBuild = new StringBuilder(driver.getPageSource());
		try { 
			Assert.assertTrue(sBuild.toString().contains("How can we contact you"));
		}catch(AssertionError aE) { 
			System.out.println(testNameLocal + ":\tfail-fast; check contact info page");
			throw aE;
		}finally {
			sBuild.delete(0, sBuild.length());
			sBuild = null;
		}
		ContactInfoPage pageUnderTest = new ContactInfoPage(driver);
		pageUnderTest.getFieldEmail_address().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		String emailToUse = "Foo@Boo.com"; // legal == the only requirement here
		pageUnderTest.getFieldEmail_address().sendKeys(emailToUse);
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		String captured = pageUnderTest.getElementForHintTextEmail().getText().trim();
		String expected = G_4115_Email_B.getHintEmailHappy();
		try { 
			Assert.assertEquals(captured, expected);
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tPASS");
			System.out.println(testNameLocal + ":\tExpected '" + expected + "\'");
			System.out.println(testNameLocal + ":\tCaptured '" + captured + "\'");
			throw aE;
		}finally { 
			FieldValidationUtilities.refreshBetweenTest(method.getName(), driver);
			captured = null;
			expected = null;
			emailToUse = null;
			testNameLocal = null;
			pageUnderTest = null;
			waiter = null;
		}
	}


	/**
	 * Test the hint text for the email field as expected 
	 * after a missing period. Negative case.
	 * <p>
	 * @throws InterruptedException
	 */
	@Test(enabled=true)
	public void testEmailMissingPeriod(Method method) {
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		setDriverTypeForTestClass(DriverTypeSauce.MAC_FIREFOX);
		driver = getPreparedDriver(testNameLocal);
		Waiter waiter = new Waiter();
		waiter.get(FV_ContactInfoTests.getStartingURL(), driver, 60);
		StringBuilder sBuild = new StringBuilder(driver.getPageSource());
		try { 
			Assert.assertTrue(sBuild.toString().contains("How can we contact you"));
		}catch(AssertionError aE) { 
			System.out.println(testNameLocal + ":\tfail-fast; check contact info page");
			throw aE;
		}finally {
			sBuild.delete(0, sBuild.length());
			sBuild = null;
		}
		ContactInfoPage pageUnderTest = new ContactInfoPage(driver);
		pageUnderTest.getFieldEmail_address().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		String emailToUse = "test@testcom"; // illegal
		pageUnderTest.getFieldEmail_address().sendKeys(emailToUse);
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldAreaCode().click();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal); // click-out
		WebElement temp = driver.findElement(By.xpath("//pg-email-address[@id='email']/div/pg-field/div[2]/span[4]"));
		Assert.assertNotNull(temp); // fast-fail
		String captured = temp.getText().trim();
		String expected = G_4115_Email_B.getHintEmailMissingPeriod();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		try { 
			Assert.assertEquals(captured, expected);
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\tExpected '" + expected + "\'");
			System.out.println(testNameLocal + ":\tCaptured '" + captured + "\'");
			throw aE;
		}finally { 
			FieldValidationUtilities.refreshBetweenTest(testNameLocal, driver);
			captured = null;
			expected = null;
			emailToUse = null;
			temp = null;
			testNameLocal = null;
			pageUnderTest = null;
			waiter = null;
		}
	}	


	/**
	 * Test the hint text for the email field as expected after an
	 * address containing a space character is entered. Negative case.
	 * <p>
	 * @throws InterruptedException
	 */
	@Test(enabled=true)
	public void testEmailContainsSpace(Method method) {
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		String emailToUse = "te st@Acme.com"; // illegal
		setDriverTypeForTestClass(DriverTypeSauce.MAC_CHROME);
		driver = getPreparedDriver(testNameLocal);
		Waiter waiter = new Waiter();
		waiter.get(FV_ContactInfoTests.getStartingURL(), driver, 60);
		StringBuilder sBuild = new StringBuilder(driver.getPageSource());
		try { 
			Assert.assertTrue(sBuild.toString().contains("How can we contact you"));
		}catch(AssertionError aE) { 
			System.out.println(testNameLocal + ":\tfail-fast; check contact info page");
			throw aE;
		}finally {
			sBuild.delete(0, sBuild.length());
			sBuild = null;
		}
		ContactInfoPage pageUnderTest = new ContactInfoPage(driver);
		pageUnderTest.getFieldEmail_address().sendKeys(emailToUse);
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldAreaCode().click();
		FieldValidationUtilities.sleepSecond(testNameLocal);
		String captured = driver.findElement(By.xpath("//pg-email-address[@id='email']/div/pg-field/div[2]/span[5]")).getText().trim();
		String expected = G_4115_Email_B.getHintEmailContainsSpace();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		try { 
			Assert.assertEquals(captured, expected);
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\tExpected '" + expected + "\'");
			System.out.println(testNameLocal + ":\tCaptured '" + captured + "\'");
			throw aE;
		}finally { 
			FieldValidationUtilities.refreshBetweenTest(method.getName(), driver);
			captured = null;
			expected = null;
			emailToUse = null;
			testNameLocal = null;
			pageUnderTest = null;
			waiter = null;
		}
	}	

	/**
	 * Test the hint text for the email field as expected after an
	 * address containing invalid characters is entered. Negative case.
	 * <p>
	 * @throws InterruptedException
	 */
	@Test(enabled=true)
	public void testEmailContainsInvalidCharacters(Method method) {
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		setDriverTypeForTestClass(DriverTypeSauce.WINDOWS_CHROME);
		driver = getPreparedDriver(testNameLocal);
		Waiter waiter = new Waiter();
		waiter.get(FV_ContactInfoTests.getStartingURL(), driver, 60);
		StringBuilder sBuild = new StringBuilder(driver.getPageSource());
		try { 
			Assert.assertTrue(sBuild.toString().contains("How can we contact you"));
		}catch(AssertionError aE) { 
			System.out.println(testNameLocal + ":\tfail-fast; check contact info page");
			throw aE;
		}finally {
			sBuild.delete(0, sBuild.length());
			sBuild = null;
		}
		ContactInfoPage pageUnderTest = new ContactInfoPage(driver);
		pageUnderTest.getFieldEmail_address().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		String emailToUse = "*%$#!?@test.com"; // illegal
		pageUnderTest.getFieldEmail_address().sendKeys(emailToUse);
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		pageUnderTest.getFieldAreaCode().click();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		String captured = driver.findElement(By.cssSelector(".pg-field-error:nth-child(7)")).getText();
		String expected = G_4115_Email_B.getHintEmailContainsInvalidCharacters();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		try { 
			Assert.assertEquals(captured, expected);
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\tExpected '" + expected + "\'");
			System.out.println(testNameLocal + ":\tCaptured '" + captured + "\'");
			throw aE;
		}finally { 
			FieldValidationUtilities.refreshBetweenTest(testNameLocal, driver);
			captured = null;
			expected = null;
			emailToUse = null;
			testNameLocal = null;
			pageUnderTest = null;
			waiter = null;
		}
	}	


	/**
	 * Test the hint text for the email field as expected after an
	 * address with incomplete dot com is entered. Negative case.
	 * <p>
	 * @throws InterruptedException
	 */
	@Test(enabled=true)
	public void testEmailContainsBadDotCom(Method method) throws InterruptedException {
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		setDriverTypeForTestClass(DriverTypeSauce.WINDOWS_FIREFOX);
		driver = getPreparedDriver(testNameLocal);
		Waiter waiter = new Waiter();
		waiter.get(FV_ContactInfoTests.getStartingURL(), driver, 60);
		StringBuilder sBuild = new StringBuilder(driver.getPageSource());
		try { 
			Assert.assertTrue(sBuild.toString().contains("How can we contact you"));
		}catch(AssertionError aE) { 
			System.out.println(testNameLocal + ":\tfail-fast; check contact info page");
			throw aE;
		}finally {
			sBuild.delete(0, sBuild.length());
			sBuild = null;
		}
		ContactInfoPage pageUnderTest = new ContactInfoPage(driver);
		pageUnderTest.getFieldEmail_address().clear();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		String emailToUse = "test@test.c"; // illegal
		pageUnderTest.getFieldEmail_address().sendKeys(emailToUse);
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldAreaCode().click();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		String captured = driver.findElement(By.cssSelector(".pg-field-error:nth-child(8)")).getText();
		String expected = G_4115_Email_B.getHintEmailContainsIncompleteDotCom();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		try { 
			Assert.assertEquals(captured, expected);
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\tExpected '" + expected + "\'");
			System.out.println(testNameLocal + ":\tCaptured '" + captured + "\'");
			throw aE;
		}finally { 
			FieldValidationUtilities.refreshBetweenTest(testNameLocal, driver);
			captured = null;
			expected = null;
			emailToUse = null;
			testNameLocal = null;
			pageUnderTest = null;
			waiter = null;
		}
	}		

	/**
	 * If subclass of TestBaseSauce, pass the test result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit driver.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		SessionId session = ((RemoteWebDriver)driver).getSessionId();
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) { 
			System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
			// could be null in a happy path run
			if(!(null==session)) { // don't use .equals() here
				((JavascriptExecutor) driver).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
			}
		} else { 
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		if(!(null==driver)) { 
			driver.quit();
		}
		System.out.println(methodNameLocal + ":\tEND tearDown.");
	}

}
