package fieldvalidations.contactinfopage;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import pages.ContactInfoPage;
import utilities.saucelabs.TestBaseSauce;
import waiter.Waiter;

/**
 * Field validation test: Contact Info page: email values.
 * <p>
 * Postive and negative cases.
 * <p>
 * Full coverage for the TestLink case below:
 * <p>
 * http://slc-qaswebtli01/testlink/linkto.php?tprojectPrefix=G&item=testcase&id=G-4115
 * <p>
 * Version 1.0 2019-01-23
 * <p>
 * Version 1.1 2019-04-19 abstract out common behavior and tune for performance
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.1
 * @see ContactInfoPage
 */
public class G_4115_Email_C extends TestBaseSauce {

	private WebDriver driver;

	/**
	 * Expected text after a valid email address is entered, or 
	 * after a simple click into the email address field.
	 * <p>
	 * @return Expected text of a field label.
	 */
	public static final String getHintEmailNull() { 
		return "Your email address is required to process the application.";
	}




	/**
	 * Test the hint text for the email field as expected 
	 * after a null entry. Negative case.
	 * <p>
	 * To spawn the expected condition we click once in the 
	 * email field then once in any other field, leaving 
	 * email blank.
	 * <p>
	 */
	@Test(enabled=true)
	public void testEmailNull(Method method) {
		String testNameLocal = getClass().getSimpleName() + "." + method.getName();
		driver = getPreparedDriver(testNameLocal);
		Waiter waiter = new Waiter();
		waiter.get(FV_ContactInfoTests.getStartingURL(), driver, 60);
		StringBuilder sBuild = new StringBuilder(driver.getPageSource());
		try { 
			Assert.assertTrue(sBuild.toString().contains("How can we contact you"));
		}catch(AssertionError aE) { 
			System.out.println(testNameLocal + ":\tfail-fast; check contact info page");
			throw aE;
		}finally {
			sBuild.delete(0, sBuild.length());
			sBuild = null;
		}
		ContactInfoPage pageUnderTest = new ContactInfoPage(driver);
		pageUnderTest.getFieldEmail_address().clear();
		FieldValidationUtilities.sleepSecond(testNameLocal);
		pageUnderTest.getFieldEmail_address().click();
		FieldValidationUtilities.sleepSecond(testNameLocal);
		pageUnderTest.getFieldAreaCode().click();
		FieldValidationUtilities.sleepSecond(testNameLocal);
		String captured = driver.findElement(By.className("pg-field-error")).getText().trim();
		String expected = G_4115_Email_C.getHintEmailNull();
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
		try { 
			Assert.assertEquals(captured, expected);
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL");
			System.out.println(testNameLocal + ":\tExpected '" + expected + "\'");
			System.out.println(testNameLocal + ":\tCaptured '" + captured + "\'");
			throw aE;
		}finally { 
			captured = null;
			expected = null;
			testNameLocal = null;
			pageUnderTest = null;
			waiter = null;
		}
	}



	/**
	 * If subclass of TestBaseSauce, pass the test result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit driver.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		SessionId session = ((RemoteWebDriver)driver).getSessionId();
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) { 
			System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
			// could be null in a happy path run
			if(!(null==session)) { // don't use .equals() here
				((JavascriptExecutor) driver).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
			}
		} else { 
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		if(!(null==driver)) { 
			driver.quit();
		}
		System.out.println(methodNameLocal + ":\tEND tearDown.");
	}

}
