package fieldvalidations.contactinfopage;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import common.IssuingState;
import common.ProgressiveLeasingConstants;
import pages.ContactInfoPage;
import utilities.CommonDriversLocal;
import utilities.DriverTypeLocal;
import waiter.Waiter;

/**
 * There's no test case in TestLink for this but we know we need to test it.
 * <p>
 * Customers in Minnesota, New Jersey, Vermont, and Wisconsin should see an error message.
 * <p>
 * Version 1.0 2019-02-21
 * <p>
 * Version 1.1 2019-02-21 parameterize the states and interate into a test engine
 * <p>
 * @author <a href="mailto:jay.loew@progleasing.com">Allen</a>
 * @author <a href="mailto:shawn.hartke@progleasing.com">Shawn</a>
 * @author <a href="mailto:christopher.rains@progleasing.com">CJ Rains</a>
 * @version 1.1
 */
public class ProhibitedStates {

	private WebDriver driver;

	private Waiter waiter;

	private ContactInfoPage pageUnderTest;

	@SuppressWarnings("static-access")
	@BeforeMethod
	@Parameters({"driverTypeIn"})
	public void setUpNotStatic(Method method, String driverTypeIn) {

		waiter = new Waiter();

		// Specify a driver here in the test class...
		//driver = new CommonDriversLocal(DriverTypesLocal.CHROME).getPreparedDriver();

		// Specify a driver in the XML suite file...
		if(driverTypeIn.equals(driverTypeIn.valueOf(DriverTypeLocal.CHROME))) { 
			driver = new CommonDriversLocal(DriverTypeLocal.CHROME).getPreparedDriver();

		} else if(driverTypeIn.equals(driverTypeIn.valueOf(DriverTypeLocal.FIREFOX))) { 
			driver = new CommonDriversLocal(DriverTypeLocal.FIREFOX).getPreparedDriver();

		} else if(driverTypeIn.equals(driverTypeIn.valueOf(DriverTypeLocal.CHROME_HEADLESS))) { 
			driver = new CommonDriversLocal(DriverTypeLocal.CHROME_HEADLESS).getPreparedDriver();

		} else if(driverTypeIn.equals(driverTypeIn.valueOf(DriverTypeLocal.FIREFOX_HEADLESS))) { 
			driver = new CommonDriversLocal(DriverTypeLocal.FIREFOX_HEADLESS).getPreparedDriver();

		} else { 
			throw new IllegalArgumentException(method.getName() + ":\tNo such driver type");
		}

		// The code in question could be tested on 8, 12, 13, or storedemo. It's on all environments now.
		waiter.get(ProgressiveLeasingConstants.URL_CONTACT_INFO_13, driver, 30);

		pageUnderTest = new ContactInfoPage(driver);		
	}

	
	@Test(enabled=true)
	public void testProhibitedStateWI(Method method) {

		String[] WI = {"000@test.com", "6082621408", "1220 Linden Dr", null, "Madison", "WI", "53706"};	

		try { 
			Assert.assertTrue(testProhibitedState(WI, IssuingState.WI));
		}catch(AssertionError aE) { 
			throw aE;
		}
	}
	
	@Test(enabled=true)
	public void testProhibitedStateMN(Method method) {

		String[] MN = {"000@test.com", "2187304394", "411 West First St", null, "Duluth", "MN", "55802"};	
		
		try { 
			Assert.assertTrue(testProhibitedState(MN, IssuingState.MN));
		}catch(AssertionError aE) { 
			throw aE;
		}
	}

	
	@Test(enabled=true)
	public void testProhibitedStateNJ(Method method) {

		String[] NJ = {"000@test.com", "6099893532", "319 East State St", null, "Trenton", "NJ", "08608"};	
		
		try { 
			Assert.assertTrue(testProhibitedState(NJ, IssuingState.NJ));
		}catch(AssertionError aE) { 
			throw aE;
		}
	}
	

	
	@Test(enabled=true)
	public void testProhibitedStateVT(Method method) {

		String[] MN = {"000@test.com", "8028657000", "149 Church St", null, "Burlington", "VT", "05401"};	
		
		try { 
			Assert.assertTrue(testProhibitedState(MN, IssuingState.VT));
		}catch(AssertionError aE) { 
			throw aE;
		}
	}
	
	
	private boolean testProhibitedState(String[] addressUnderTest, IssuingState enumState) {

		String testNameLocal = new Exception().getStackTrace()[0].getMethodName();

		boolean isPass = false;

		WebElement fieldEmailTemp = driver.findElement(By.id("email-input"));
		Assert.assertNotNull(fieldEmailTemp);
		fieldEmailTemp.sendKeys(addressUnderTest[0]);
		FieldValidationUtilities.sleepSecond(testNameLocal);
//		pageUnderTest.getFieldEmail_address().sendKeys(addressUnderTest[0]);
		
		pageUnderTest.getFieldAreaCode().sendKeys(addressUnderTest[1].substring(0, 3));

		pageUnderTest.getFieldPrefix().sendKeys(addressUnderTest[1].substring(3, 6));
		
		Actions actions = new Actions(driver);
		actions.sendKeys(Keys.PAGE_DOWN).build().perform();
		FieldValidationUtilities.sleepSecond(testNameLocal);

		pageUnderTest.getFieldLineNumber().sendKeys(addressUnderTest[1].substring(6, 10));

		pageUnderTest.getFieldAddressLine1().sendKeys(addressUnderTest[2]);

		if(!(null==addressUnderTest[3])) { 
			pageUnderTest.getFieldAddressLine2().sendKeys(addressUnderTest[3]);
		} else { 
			System.out.println(testNameLocal + ":\taddress line two skipped intentionally");
		}

		actions.sendKeys(Keys.PAGE_DOWN).build().perform();
		FieldValidationUtilities.sleepSecond(testNameLocal);
		actions = null;
		
		pageUnderTest.getFieldCity().sendKeys(addressUnderTest[4]);

		pageUnderTest.getFieldState().sendKeys(addressUnderTest[5]);

		pageUnderTest.getFieldZIP().sendKeys(addressUnderTest[6]);

		// scroll down after ZIP field else false-fail at the checkbox
		JavascriptExecutor jse = (JavascriptExecutor)driver;
		jse.executeScript("window.scrollBy(0,250)", "");
		jse = null;

		pageUnderTest.acceptSpam();
		
		// optional, if you're watching...you can delete this one if you prefer
		FieldValidationUtilities.sleepSecond(testNameLocal);
		FieldValidationUtilities.sleepSecond(testNameLocal);
		
		String stateNameLong = enumState.properName;
		String expected = "Unfortunately, we are unable to lease products to customers residing in " + stateNameLong + ".";
		String captured = driver.findElement(By.cssSelector(".pg-address-error:nth-child(14)")).getText();
		// //div[2]/span[14]

		if(expected.equals(captured)) { 
			isPass = true;
			System.out.println("\n" + testNameLocal + ":\tPASS with " + addressUnderTest[5]);
		} else { 
			System.out.println("\n" + testNameLocal + ":\tFAIL with " + addressUnderTest[5]);
			System.out.println(testNameLocal + ":\texpected \'" + expected + "\'");
			System.out.println(testNameLocal + ":\tcaptured \'" + captured + "\'");
		}

		addressUnderTest = null;
		stateNameLong = null;
		expected = null;
		captured = null;
		testNameLocal = null;

		return isPass;
	}

	@AfterMethod
	public void afterMethod() {
		waiter = null;
		pageUnderTest = null;
		driver.quit();
	}

}
