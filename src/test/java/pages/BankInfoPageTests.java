package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import common.ProgressiveLeasingConstants;
import utilities.saucelabs.DriverTypeSauce;
import utilities.saucelabs.TestBaseSauce;

public class BankInfoPageTests extends TestBaseSauce {

	private static final String uRLUnderTest = ProgressiveLeasingConstants.URL_BANK_INFO_12;

	private static final DriverTypeSauce driverUnderTest = DriverTypeSauce.WINDOWS_CHROME;

	private static boolean isPassPrimary = false;

	private WebDriver driver;

	private BankInfoPage pageUnderTest;

	public void setupNotStatic(String testNameIn) {
		if(isPassPrimary) { 
			return;
		}
		super.setDriverTypeForTestClass(driverUnderTest);
		driver = getPreparedDriver(testNameIn);
		driver.navigate().to(uRLUnderTest);
		try { 
			new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.id("input-routing-number")));
		}catch(TimeoutException tE) { 
			throw new IllegalStateException("FAST-FAIL from setup with " + tE.getClass().getSimpleName(),tE);
		}catch(Exception anyOther) { 
			throw new IllegalStateException("FAST-FAIL from setup with " + anyOther.getClass().getSimpleName(),anyOther);
		}
		pageUnderTest = new BankInfoPage(driver);
		System.out.println("setup complete");
	}

	@Test(enabled=true,priority=1)
	public void testHappyPathPopulateSubmit() {
		String testNameLocal = new Exception().getStackTrace()[0].getMethodName();
		setupNotStatic(testNameLocal);
		if(isPassPrimary) { 
			return;
		}
		try { 
			pageUnderTest.happyPathPopulate();
		}catch(Exception anyException) { 
			throw new AssertionError(anyException);
		}
		Actions actions = new Actions(driver);
		actions.sendKeys(Keys.PAGE_DOWN).build().perform();
		FieldValidationUtilities.sleepSecond(testNameLocal);
		try { 
			pageUnderTest.getButtonContinue_to_apply().click();
			BankInfoPageTests.isPassPrimary = true;
		}catch(Exception anyException) { 
			throw new AssertionError(anyException);
		}finally { 
			testNameLocal = null;
			actions = null;
		}
	}

	@Test(enabled=true,priority=2)
	public void testHappyPathPopulate() {
		String testNameLocal = new Exception().getStackTrace()[0].getMethodName();
		setupNotStatic(testNameLocal);
		if(isPassPrimary) { 
			throw new SkipException(testNameLocal + " skipped intentionally");
		}
		try { 
			pageUnderTest.happyPathPopulate();
		}catch(Exception anyException) { 
			throw new AssertionError(anyException);
		}
	}

	@Test(enabled=true,priority=2)
	public void testHappyPathPopulateWithArgs() {
		String testNameLocal = new Exception().getStackTrace()[0].getMethodName();
		if(isPassPrimary) { 
			throw new SkipException(testNameLocal + " skipped intentionally");
		}
		setupNotStatic(testNameLocal);

		try { 
			pageUnderTest.happyPathPopulate("123456", "9000");
		}catch(Exception anyException) { 
			throw new AssertionError(anyException);
		}
	}

	/**
	 * If subclass of TestBaseSauce, pass the test result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit driver.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) {
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		SessionId session = ((RemoteWebDriver)driver).getSessionId();
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) { 
			System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
			// could be null in a happy path run
			if(!(null==session)) { // don't use .equals() here
				((JavascriptExecutor) driver).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
			}
		} else { 
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		if(!(null==driver)) { 
			driver.quit();
		}
		System.out.println(methodNameLocal + ":\tEND tearDown.");
	}
}
