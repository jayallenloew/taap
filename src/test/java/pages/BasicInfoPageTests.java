/*
 * Copyright Progressive Leasing LLC.
 */
package pages;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import common.ProgressiveLeasingConstants;
import utilities.CommonDriversLocal;
import utilities.DeleteSessionCookie;
import utilities.DriverTypeLocal;

/**
 * Test class for page object for the "Basic info" page. 
 * <p>
 * Path: 
 * <p>
 * Store demo page > No Credit Needed<BR>Find out more | Marketing page > Get started | Request code page > Enter number, accept terms, Get started | Enter the code | Basic info
 * <p>
 * Version 1.1 2019-03-26 hardening and automatic cleanup
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @author <a href="mailto:christopher.rains@progleasing.com">CJ Rains</a>
 * @version 1.1
 * @see BasicInfoPage.java
 */
public class BasicInfoPageTests {

	private WebDriver driver;

	private BasicInfoPage pageUnderTest;

	private static boolean isSSNClickThroughPass = false;

	/**
	 * This value will be set during test execution. Then passed to 
	 * a cleanup utility during teardown.
	 */
	private String cookieToDelete;

	/**
	 * In a couple places, this test will require a test email address.
	 * <p>
	 * Current possible: 000@test.com through 010@test.com
	 */
	public static final String EMAIL_FOR_THIS_TEST = "002@test.com";
	
	@SuppressWarnings("static-access")
	@BeforeMethod
	@Parameters({"driverTypeLocal"})
	public void setUp(Method method, String driverTypeIn) throws InterruptedException {
		
		if(isSSNClickThroughPass) { 
			return;
		}

		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();

		// Specify a driver here in the test class...
//		driver = new CommonDriversLocal(DriverTypeLocal.CHROME).getPreparedDriver();

		// Specify a driver in the XML suite file...
		if(driverTypeIn.equals(driverTypeIn.valueOf(DriverTypeLocal.CHROME))) { 
			driver = new CommonDriversLocal(DriverTypeLocal.CHROME).getPreparedDriver();
			
		} else if(driverTypeIn.equals(driverTypeIn.valueOf(DriverTypeLocal.FIREFOX))) { 
			driver = new CommonDriversLocal(DriverTypeLocal.FIREFOX).getPreparedDriver();
		
		} else if(driverTypeIn.equals(driverTypeIn.valueOf(DriverTypeLocal.CHROME_HEADLESS))) { 
			throw new IllegalArgumentException(driverTypeIn + " unsupported.");
			
		} else if(driverTypeIn.equals(driverTypeIn.valueOf(DriverTypeLocal.FIREFOX_HEADLESS))) { 
			driver = new CommonDriversLocal(DriverTypeLocal.FIREFOX_HEADLESS).getPreparedDriver();
			
		} else { 
			throw new IllegalArgumentException(method.getName() + ":\tNo such driver type");
		}

		// go to demo page
		driver.navigate().to(ProgressiveLeasingConstants.URL_STORE_DEMO);

		// wait for Find out more button 
		try { 
//			new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//img[@alt='Learn more about Progressive Leasing']")));
			new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#prog-valprop > img")));
			System.out.println(methodNameLocal + ":\tstore demo landing page... (OK)");
		}catch(TimeoutException tE) { 
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check store demo page."), tE);
		}

		StoreDemoLandingPage demoPageTemp = new StoreDemoLandingPage(driver);

		// click through to marketing page
		demoPageTemp.getButtonFindOutMore().click(); 
		demoPageTemp = null;

		// starting from storedemo (which is required in this test) you'll always land on 12
		// force redirect from 12 to 13
		StringBuilder sBuild = new StringBuilder(driver.getCurrentUrl());
		int indexBegin = sBuild.toString().indexOf("qaswebapp");
		sBuild.replace(indexBegin, (indexBegin+11), "qaswebapp13");
		driver.navigate().to(sBuild.toString());

		// wait for Get Started button 
		try { 
			new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@type='cta']")));
			System.out.println(methodNameLocal + ":\tmarketing page... (OK)");
		}catch(TimeoutException tE) { 
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check marketing page."), tE);
		}

		MarketingPage marketingPage = new MarketingPage(driver);

		// click through to get started request code
		marketingPage.getButtonGet_started().click(); 
		FieldValidationUtilities.sleepSecond(methodNameLocal);
		FieldValidationUtilities.sleepSecond(methodNameLocal);
		marketingPage = null;

		// wait for Mobile phone or email field 
		try { 
			//			new WebDriverWait(driver, 20).until(ExpectedConditions.presenceOfElementLocated(By.id("otp-contact-input")));
			new WebDriverWait(driver, 20).until(ExpectedConditions.presenceOfElementLocated(By.id("pg-field-contact")));
			System.out.println(methodNameLocal + ":\tget started request code page... (OK)");
		}catch(TimeoutException tE) { 
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check get started request code page."), tE);
		}
		
		GetStartedRequestCodePage requestCodeTemp = new GetStartedRequestCodePage(driver);

		// enter a test email address or phone
		driver.findElement(By.id("pg-field-contact")).sendKeys(EMAIL_FOR_THIS_TEST);
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);

		// accept the terms in order to proceed to enter the code
		requestCodeTemp.getCheckboxTerms().click(); // accept terms
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);

		// after the checkbox, 5 tabs and a space to advance to the next page
		Actions builder = new Actions(driver);
		builder.sendKeys(Keys.TAB).build().perform();
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
		builder.sendKeys(Keys.TAB).build().perform();
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
		builder.sendKeys(Keys.TAB).build().perform();
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
		builder.sendKeys(Keys.TAB).build().perform();
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
		builder.sendKeys(Keys.TAB).build().perform();
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);
		builder.sendKeys(Keys.SPACE).build().perform();
		FieldValidationUtilities.sleepQuarterSecond(methodNameLocal);

		FieldValidationUtilities.sleepSecond(methodNameLocal);

		requestCodeTemp = null;
		builder = null;
		
		// wait for enter the code field 
		try { 
			new WebDriverWait(driver, 20).until(ExpectedConditions.presenceOfElementLocated(By.id("pg-field-code")));
			System.out.println(methodNameLocal + ":\tenter the code page... (OK)");
		}catch(TimeoutException tE) { 
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check enter the code page."), tE);
		}

		// enter the (test) code and take a nap, unfortunately
		driver.findElement(By.id("pg-field-code")).sendKeys("000000");
		// yeah the sleeps suck, thank Magento, move on....
		try { 
			Thread.sleep(5000); // Don't remove or reduce. It's not our issue.
		}catch(InterruptedException iE) { 
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup"), iE);
		}

		// wait for basic info page
		try { 
			new WebDriverWait(driver, 60).until(ExpectedConditions.presenceOfElementLocated(By.id("first-name-input")));
			captureSessionCookieToDelete(); // automatically delete the session cookie which by this time exists
			System.out.println(methodNameLocal + ":\tbasic info page... (OK)");
		}catch(TimeoutException tE) { 
			System.out.println(methodNameLocal + ":\ttest user " + EMAIL_FOR_THIS_TEST + " may be dirty");
			throw new AssertionError((methodNameLocal + ":\tfast-fail in setup; check basic info page."), tE);
		}
		
		pageUnderTest = new BasicInfoPage(driver);
		System.out.println(method.getName() + ":\t setup complete....");
	}


	@Test(enabled=true,priority=1)
	public void testFieldNames(Method method) { 

		WebElement fieldTemp = pageUnderTest.getFieldNameFirst();
		
		Assert.assertNotNull(fieldTemp, (method.getName() + ":\tCouldn't instantiate the first name field."));
		
		fieldTemp.clear();
		
		FieldValidationUtilities.sleepQuarterSecond(method.getName());
		
		try { 
			fieldTemp.sendKeys("Mary Jo"); // any valid name
			FieldValidationUtilities.sleepQuarterSecond(method.getName());
			fieldTemp.clear();
			FieldValidationUtilities.sleepQuarterSecond(method.getName());
			System.out.println("\n" + method.getName() + ":\tPASS - first name");
		}catch(Exception anyExceptionWhatsoever) { 
			System.out.println("\n" + method.getName() + ":\tFAIL - first name");
			throw new AssertionError(method.getName(),anyExceptionWhatsoever);
		}
		
		fieldTemp = null;
		
		fieldTemp = pageUnderTest.getFieldNameLast();
		
		Assert.assertNotNull(fieldTemp, (method.getName() + ":\tCouldn't instantiate the last name field."));		
		try { 
			fieldTemp.sendKeys("Robinson"); // any valid name
			FieldValidationUtilities.sleepQuarterSecond(method.getName());
			fieldTemp.clear();
			FieldValidationUtilities.sleepQuarterSecond(method.getName());
			System.out.println("\n" + method.getName() + ":\tPASS - last name");
		}catch(Exception anyExceptionWhatsoever) { 
			System.out.println("\n" + method.getName() + ":\tFAIL - first name");
			throw new AssertionError(method.getName(),anyExceptionWhatsoever);
		}finally { 
			fieldTemp = null;
		}
	}


	@Test(enabled=true,priority=2)
	public void testEnterDate(Method method) { 
		
		try { 
			pageUnderTest.enterDate("09","19","1994");
			System.out.println("\n" + method.getName() + ":\tPASS");
		}catch(Exception anyException) { 
			System.out.println("\n" + method.getName() + ":\tFAIL");
			throw new AssertionError(method.getName(), anyException);
		}
	}



	@Test(enabled=true,priority=3)
	public void testEnterSSN(Method method) { 
		
		try { 
			pageUnderTest.enterSSN("552","19","1994"); // any legal value
			isSSNClickThroughPass = true;
			System.out.println("\n" + method.getName() + ":\tPASS");
		}catch(Exception anyException) { 
			System.out.println("\n" + method.getName() + ":\tFAIL");
			throw new AssertionError(method.getName(), anyException);
		}
	}
	

	@Test(enabled=true,priority=4)
	public void testSSNFields(Method method) { 
		
		if(isSSNClickThroughPass) { 
			throw new SkipException(method.getName() + " skipped intentionally");
		}
		
		WebElement underTest = pageUnderTest.getFieldSSN1_area();
		try { 
			underTest.clear();
			FieldValidationUtilities.sleepQuarterSecond(method.getName());
			underTest.sendKeys("552");
			FieldValidationUtilities.sleepQuarterSecond(method.getName());
			underTest.clear();
			FieldValidationUtilities.sleepQuarterSecond(method.getName());
			System.out.println("\n" + method.getName() + ":\tarea is good");
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError("\n" + method.getName() + ":\tFAIL on area", anyExceptionWhatsoever);
		}finally { 
			underTest = null;
		}
		
		underTest = pageUnderTest.getFieldSSN2_group();
		try { 
			underTest.clear();
			FieldValidationUtilities.sleepQuarterSecond(method.getName());
			underTest.sendKeys("19");
			FieldValidationUtilities.sleepQuarterSecond(method.getName());
			underTest.clear();
			FieldValidationUtilities.sleepQuarterSecond(method.getName());
			System.out.println("\n" + method.getName() + ":\tgroup is good");
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError("\n" + method.getName() + ":\tFAIL on group", anyExceptionWhatsoever);
		}finally { 
			underTest = null;
		}
		
		underTest = pageUnderTest.getFieldSSN3_serial();
		try { 
			underTest.clear();
			FieldValidationUtilities.sleepQuarterSecond(method.getName());
			underTest.sendKeys("1882");
			FieldValidationUtilities.sleepQuarterSecond(method.getName());
			underTest.clear();
			FieldValidationUtilities.sleepQuarterSecond(method.getName());
			System.out.println("\n" + method.getName() + ":\tserial is good");
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError("\n" + method.getName() + ":\tFAIL on serial", anyExceptionWhatsoever);
		}finally { 
			underTest = null;
		}
	}
	
	
	private void captureSessionCookieToDelete() { 
		System.out.println("\n====cookie to console====");
		java.util.Set<Cookie> cookies = driver.manage().getCookies();
		for(Cookie C : cookies) { 
			if(!(C.getName().equals("sessionId"))) { 
				continue;
			}
			cookieToDelete = C.getValue();
			System.out.print("To delete:\t" + cookieToDelete);
		}
		System.out.println("\n====cookie to console====\n");
		cookies = null;
	}
	
	
	@AfterMethod
	public void tearDown() {
		@SuppressWarnings("unused")
		DeleteSessionCookie deleteCookie = new DeleteSessionCookie(cookieToDelete,EMAIL_FOR_THIS_TEST);
		pageUnderTest = null;
		driver.quit();
	}

}
