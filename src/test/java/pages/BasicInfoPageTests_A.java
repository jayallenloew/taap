/*
 * Copyright Progressive Leasing LLC.
 */
package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import common.ProgressiveLeasingConstants;
import utilities.DeleteSessionCookie;
import utilities.ResetThisUser;
import utilities.saucelabs.TestBaseSauce;

/**
 * Test class for page object for the "Basic info" page. 
 * <p>
 * Path: 
 * <p>
 * Store demo page > No Credit Needed<BR>Find out more | Marketing page > Get started | Request code page > Enter number, accept terms, Get started | Enter the code | Basic info
 * <p>
 * Version 1.1 2019-03-26 hardening and automatic cleanup
 * <p>
 * version 2.0 2019-04-11 refactor to use test base and re-run to green
 * <p>
 * version 2.1 2019-04-29 refactor teardown for clean reporting during happy skip 
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @author <a href="mailto:christopher.rains@progleasing.com">CJ Rains</a>
 * @version 2.1
 * @see BasicInfoPage.java
 */
public class BasicInfoPageTests_A extends TestBaseSauce {

	private WebDriver driver;

	private BasicInfoPage pageUnderTest;

	private static boolean isSSNClickThroughPass = false;

	/**
	 * This value will be set during test execution. Then passed to 
	 * a cleanup utility during teardown.
	 */
	private String cookieToDelete;

	/**
	 * In a couple places, this test will require a test email address.
	 * <p>
	 * Current possible: 000@test.com through 010@test.com
	 */
	public static final String EMAIL_FOR_THIS_TEST = "004@test.com";


	private void setUpLocal(String testNameIn) {

		System.out.println(testNameIn + ":\tsetup begin... (OK)");

		@SuppressWarnings("unused")
		ResetThisUser resetTemp = new ResetThisUser(EMAIL_FOR_THIS_TEST);
		FieldValidationUtilities.sleepSecond(testNameIn);
		resetTemp = null;

		driver = getPreparedDriver(testNameIn);

		// add fast-fail check for null driver here due to teardown
		Assert.assertNotNull(driver);
		System.out.println(testNameIn + ":\tdriver instantiated... (OK)");

		// go to demo page
		driver.navigate().to(ProgressiveLeasingConstants.URL_STORE_DEMO);

		// wait for Find out more button 
		try { 
			new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("img[alt=\"Learn more about Progressive Leasing\"]")));
			System.out.println(testNameIn + ":\tstore demo landing page... (OK)");
		}catch(TimeoutException tE) { 
			throw new AssertionError((testNameIn + ":\tfast-fail in setup; check store demo page."), tE);
		}

		StoreDemoLandingPage demoPageTemp = new StoreDemoLandingPage(driver);

		// click through to marketing page
		demoPageTemp.getButtonFindOutMore().click(); 
		demoPageTemp = null;


		/*
		 * The redirects below are considered temporary.
		 * We'll do the "real" work under a separate 
		 * ticket after we've clarified requirements.
		 */
		
		// force redirect from 12 to 13
//		StringBuilder sBuild = new StringBuilder(driver.getCurrentUrl());
//		if(sBuild.toString().contains("qaswebapp12")) { 
//			System.out.println(testNameIn + ":\tbegin force redirect from 12 onto 13... (OK)");
//			int indexBegin = sBuild.toString().indexOf("qaswebapp");
//			sBuild.replace(indexBegin, (indexBegin+11), "qaswebapp13");
//			driver.navigate().to(sBuild.toString());
//			sBuild = null;
//		}

		// force redirect from 13 to 12
//		StringBuilder sBuild2 = new StringBuilder(driver.getCurrentUrl());
//		if(sBuild2.toString().contains("qaswebapp13")) { 
//			System.out.println(testNameIn + ":\tbegin force redirect from 13 onto 12... (OK)");
//			int indexBegin = sBuild2.toString().indexOf("qaswebapp");
//			sBuild2.replace(indexBegin, (indexBegin+11), "qaswebapp12");
//			driver.navigate().to(sBuild2.toString());
//			sBuild2 = null;
//		}

		// force redirect from demo1 to 13
//		StringBuilder sBuild3 = new StringBuilder(driver.getCurrentUrl());
//		if(sBuild3.toString().contains("demo1.progressivelp.com")) { 
//			System.out.println(testNameIn + ":\tbegin force redirect from demo1 onto 13... (OK)");
//			int indexBegin = sBuild3.toString().indexOf("demo1.progressivelp.com");
//			sBuild3.replace(indexBegin, (indexBegin+31), "vdc-qaswebapp13.stormwind.local");
//			driver.navigate().to(sBuild3.toString());
//			sBuild3 = null;
//		}

		// force redirect from demo2 to 13

		
		// wait for Get Started button 
		try { 
			new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@type='cta']")));
			System.out.println(testNameIn + ":\tmarketing page... (OK)");
		}catch(TimeoutException tE) { 
			throw new AssertionError((testNameIn + ":\tfast-fail in setup; check marketing page."), tE);
		}

		MarketingPage marketingPage = new MarketingPage(driver);

		// click through to get started request code
		marketingPage.getButtonGet_started().click(); 
		FieldValidationUtilities.sleepSecond(testNameIn);
		FieldValidationUtilities.sleepSecond(testNameIn);
		marketingPage = null;

		// wait for Mobile phone or email field 
		try { 
			new WebDriverWait(driver, 20).until(ExpectedConditions.presenceOfElementLocated(By.id("pg-field-contact")));
			System.out.println(testNameIn + ":\tget started request code page... (OK)");
		}catch(TimeoutException tE) { 
			throw new AssertionError((testNameIn + ":\tfast-fail in setup; check get started request code page."), tE);
		}

		GetStartedRequestCodePage requestCodeTemp = new GetStartedRequestCodePage(driver);

		// enter a test email address or phone
		driver.findElement(By.id("pg-field-contact")).sendKeys(EMAIL_FOR_THIS_TEST);
		FieldValidationUtilities.sleepQuarterSecond(testNameIn);

		// accept the terms in order to proceed to enter the code
		requestCodeTemp.getCheckboxTerms().click(); // accept terms
		FieldValidationUtilities.sleepQuarterSecond(testNameIn);

		// after the checkbox, 5 tabs and a space to advance to the next page
		Actions builder = new Actions(driver);
		builder.sendKeys(Keys.TAB).build().perform();
		FieldValidationUtilities.sleepQuarterSecond(testNameIn);
		builder.sendKeys(Keys.TAB).build().perform();
		FieldValidationUtilities.sleepQuarterSecond(testNameIn);
		builder.sendKeys(Keys.TAB).build().perform();
		FieldValidationUtilities.sleepQuarterSecond(testNameIn);
		builder.sendKeys(Keys.TAB).build().perform();
		FieldValidationUtilities.sleepQuarterSecond(testNameIn);
		builder.sendKeys(Keys.TAB).build().perform();
		FieldValidationUtilities.sleepQuarterSecond(testNameIn);
		builder.sendKeys(Keys.SPACE).build().perform();
		FieldValidationUtilities.sleepQuarterSecond(testNameIn);

		FieldValidationUtilities.sleepSecond(testNameIn);

		requestCodeTemp = null;
		builder = null;

		// wait for enter the code field 
		try { 
			new WebDriverWait(driver, 20).until(ExpectedConditions.presenceOfElementLocated(By.id("pg-field-code")));
			System.out.println(testNameIn + ":\tenter the code page... (OK)");
		}catch(TimeoutException tE) { 
			throw new AssertionError((testNameIn + ":\tfast-fail in setup; check enter the code page."), tE);
		}


		// enter the (test) code and take a nap, unfortunately
		driver.findElement(By.id("pg-field-code")).sendKeys("000000");
		// yeah the sleeps suck, thank Magento, move on....
		try { 
			Thread.sleep(5000); // Don't remove or reduce. It's not our issue.
		}catch(InterruptedException iE) { 
			throw new AssertionError((testNameIn + ":\tfast-fail in setup"), iE);
		}

		// wait for basic info page
		try { 
			new WebDriverWait(driver, 60).until(ExpectedConditions.presenceOfElementLocated(By.id("first-name-input")));
			captureSessionCookieToDelete(); // automatically delete the session cookie which by this time exists
			System.out.println(testNameIn + ":\tbasic info page... (OK)");
		}catch(TimeoutException tE) { 
			System.out.println(testNameIn + ":\ttest user " + EMAIL_FOR_THIS_TEST + " may be dirty");
			throw new AssertionError((testNameIn + ":\tfast-fail in setup; check basic info page."), tE);
		}

		pageUnderTest = new BasicInfoPage(driver);
		System.out.println(testNameIn + ":\t setup end....");
	}


	@Test(enabled=true,priority=1)
	public void testEnterSSN() { 

		String testNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();

		setUpLocal(testNameLocal);

		System.out.println(testNameLocal + ":\t begin test proper....");

		try { 
			pageUnderTest.enterSSN("552","19","1994"); // any legal value
			isSSNClickThroughPass = true;
			System.out.println("\n" + testNameLocal + ":\tPASS");
		}catch(Exception anyException) { 
			throw new AssertionError(testNameLocal, anyException);
		}
	}



	@Test(enabled=true,priority=2)
	public void testSSNFields() { 

		String testNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();

		if(isSSNClickThroughPass) { 
			throw new SkipException(testNameLocal + " skipped intentionally");
		}

		setUpLocal(testNameLocal);

		System.out.println(testNameLocal + ":\t begin test proper....");

		WebElement underTest = pageUnderTest.getFieldSSN1_area();
		try { 
			underTest.clear();
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
			underTest.sendKeys("552");
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
			underTest.clear();
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
			System.out.println("\n" + testNameLocal + ":\tarea is good");
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError("\n" + testNameLocal + ":\tFAIL on area", anyExceptionWhatsoever);
		}finally { 
			underTest = null;
		}

		underTest = pageUnderTest.getFieldSSN2_group();
		try { 
			underTest.clear();
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
			underTest.sendKeys("19");
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
			underTest.clear();
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
			System.out.println("\n" + testNameLocal + ":\tgroup is good");
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError("\n" + testNameLocal + ":\tFAIL on group", anyExceptionWhatsoever);
		}finally { 
			underTest = null;
		}

		underTest = pageUnderTest.getFieldSSN3_serial();
		try { 
			underTest.clear();
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
			underTest.sendKeys("1882");
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
			underTest.clear();
			FieldValidationUtilities.sleepQuarterSecond(testNameLocal);
			System.out.println("\n" + testNameLocal + ":\tserial is good");
		}catch(Exception anyExceptionWhatsoever) { 
			throw new AssertionError("\n" + testNameLocal + ":\tFAIL on serial", anyExceptionWhatsoever);
		}finally { 
			underTest = null;
		}
	}



	private void captureSessionCookieToDelete() { 
		System.out.println("\n====cookie to console====");
		java.util.Set<Cookie> cookies = driver.manage().getCookies();
		for(Cookie C : cookies) { 
			if(!(C.getName().equals("sessionId"))) { 
				continue;
			}
			cookieToDelete = C.getValue();
			System.out.print("To delete:\t" + cookieToDelete);
		}
		System.out.println("\n====cookie to console====\n");
		cookies = null;
	}


	/**
	 * If subclass of TestBaseSauce, pass the test result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit driver.
	 * <p>
	 * Delete the cookie this test just created.
	 * <p>
	 * Session could potentially be null when everything's working fine, so we 
	 * do a null check here just to keep the results tab unpolluted. As of this 
	 * update, there should be one pass and one skip.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) {
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		SessionId session = ((RemoteWebDriver)driver).getSessionId();
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) { 
			System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
			// could be null in a happy path run
			if(!(null==session)) { // don't use .equals() here
				((JavascriptExecutor) driver).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
			}
		} else { 
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		if(!(null==driver)) { 
			driver.quit();
		}
		pageUnderTest = null;
		@SuppressWarnings("unused")
		DeleteSessionCookie deleteCookie = new DeleteSessionCookie(cookieToDelete,EMAIL_FOR_THIS_TEST);
		System.out.println("\n" + methodNameLocal + ":\tEND tearDown.");
	}
}
