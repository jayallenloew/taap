package pages;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Temporary class disregard for now (although it does work, and is usable).
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @author <a href="mailto:Shawn.Hartke@progleasing.com">Shawn Hartke</a>
 */
public class SessionHack {
	
	private static final String sessionID = "0077d26d-331c-44fe-a951-93099fa34dac";
	public static final String emailAddressUsed = "004@test.com";

	public static void main(String[] args) throws NoSuchAlgorithmException /*throws IOException*/, KeyManagementException {
		
		SSLContext ctx = SSLContext.getInstance("TLS");
		ctx.init(new KeyManager[0], new TrustManager[] { new DefaultTrustManager()},  new SecureRandom());
		SSLContext.setDefault(ctx);
		
		URL uRL = null;
		try {
			uRL = new URL("https://vdc-qaswebapp13.stormwind.local/ecommsession/api/v1/checkoutsession/resume/login?AuthenticationId=" + SessionHack.emailAddressUsed.substring(0, 4) + "%40test.com&SessionId=" + SessionHack.sessionID);
		} catch (MalformedURLException e1) {
			e1.printStackTrace();
		}
		HttpsURLConnection con = null;
		try {
			con = (HttpsURLConnection) uRL.openConnection();
		} catch (IOException e) {
			e.printStackTrace();
		}
		con.setHostnameVerifier(new HostnameVerifier() {
			@Override
			public boolean verify(String arg0, SSLSession arg1) { 
				return true;
			}
		});
		
		con.setRequestProperty("Authorization", "Basic cHJvY2Vzc01hbmFnZXI6KWs8TmY9MlwpeXpweWJM");
		try {
			con.setRequestMethod("DELETE");
			try {
				con.connect();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				System.out.println("con.getResponseCode():\t" + con.getResponseCode());
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			System.out.println("after DELETE");
		} catch (ProtocolException e) {
			e.printStackTrace();
		}finally { 
			con.disconnect();
			System.out.println("after disconnect()");
		}

	}
	
	private static class DefaultTrustManager implements X509TrustManager { 

		@Override
		public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
			// TODO Auto-generated method stub
			
		}

		@Override
		public X509Certificate[] getAcceptedIssuers() {
			// TODO Auto-generated method stub
			return null;
		}

}
	
}
