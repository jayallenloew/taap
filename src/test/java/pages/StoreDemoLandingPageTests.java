/*
 * Copyright Progressive Leasing LLC.
 */
package pages;

import java.util.logging.Logger;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.ProgressiveLeasingConstants;
import utilities.saucelabs.DriverTypeSauce;
import utilities.saucelabs.TestBaseSauce;
import waiter.Waiter;

/**
 * Test class for page object for store demo landing page. 
 * <p>
 * We almost no responsibility here. This is mostly a placeholder for a future day.
 * <p>
 * Version 1.4 2019-04-19
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @author <a href="mailto:christopher.rains@progleasing.com">CJ Rains</a>
 * @version 1.4
 * @see StoreDemoLandingPage.java
 */
public class StoreDemoLandingPageTests extends TestBaseSauce {

	private static final Logger LOGGER = Logger.getLogger(StoreDemoLandingPageTests.class.getName());

	private WebDriver driver;

	private StoreDemoLandingPage pageUnderTest;

	private Waiter waiter;

	private static final int TIMEOUT_MAX_SECONDS = 15;



	/**
	 * LEARN MORE button up top, or Find out more button at bottom...either one == PASS.
	 * <p>
	 * Only if both fail do we want this test to fail.
	 * <p>
	 * Update, now there is only one of those two.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true) 
	public void testButtonPrimary() { 

		String testNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();

		setDriverTypeForTestClass(DriverTypeSauce.WINDOWS_FIREFOX);

		driver = getPreparedDriver(testNameLocal);

		waiter = new Waiter();

		waiter.get(ProgressiveLeasingConstants.URL_STORE_DEMO,driver,TIMEOUT_MAX_SECONDS);

		pageUnderTest = new StoreDemoLandingPage(driver);

		System.out.println(testNameLocal + " setup complete");
		
		pageUnderTest.getButtonFindOutMore().click();

		try { 
			new WebDriverWait(driver,TIMEOUT_MAX_SECONDS).until(ExpectedConditions.titleContains("Progressive Leasing"));
			LOGGER.info("\n" + testNameLocal + " PASS");
		}catch(TimeoutException tE) { 
			LOGGER.info("\n" + testNameLocal + " PASS");
			throw new AssertionError(testNameLocal, tE);
		}

	}


	/**
	 * If subclass of TestBaseSauce, pass the test result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit driver.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {

		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		SessionId session = ((RemoteWebDriver)driver).getSessionId();
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) { 
			System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
			// could be null in a happy path run
			if(!(null==session)) { // don't use .equals() here
				((JavascriptExecutor) driver).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
			}
		} else { 
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		if(!(null==driver)) { 
			driver.quit();
		}
		waiter = null;
		System.out.println("\n" + methodNameLocal + ":\tEND tearDown.");
	}

}
