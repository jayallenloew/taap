package tapp.common;

/**
 * A straight-up array of String values.
 * <p>
 * A traditional for loop to print all values.
 * <p>
 * An enhanced for loop to print all values.
 * <p>
 * @author <a href="mailto:jay.loew@progleasing.com">Allen</a>
 * @version 1.0
 */
public class DemoArray {
	
	private String[] namesFirst = { 
			"Deepa",
			"Misty",
			"David",
			"Jessica",
			"Court",
			"Cheryl"
	};
	
	public static void main(String[] args) {
		
		DemoArray demo = new DemoArray();
		
		for(String S : demo.namesFirst) { 
			System.out.println(S);
		}
		
		System.out.println("======");
		
		for(int i = 0;i<demo.namesFirst.length;i++) { 
			System.out.println(demo.namesFirst[i]);
		}
		
//		demo.namesFirst.add("Isaac"); // no can do: compiler error
		// there's no add or remove to-from traditional array
	}

}
