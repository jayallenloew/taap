package tapp.common;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * An ArrayList of type String.
 * <p>
 * Add the same names.
 * <p>
 * Iterate through them.
 * <p>
 * @author <a href="mailto:jay.loew@progleasing.com">Allen</a>
 * @version 1.0
 */
public class DemoArrayList {
	
	private ArrayList<String> namesFirst;
	
	public static void main(String[] args) {

		DemoArrayList demo = new DemoArrayList();
		
		demo.namesFirst = new ArrayList<String>();
		
		demo.namesFirst.add("Deepa");
		demo.namesFirst.add("Misty");
		demo.namesFirst.add("David");
		demo.namesFirst.add("Jessica");
		demo.namesFirst.add("Court");
		demo.namesFirst.add("Cheryl");
		
		Iterator<String> iterate = demo.namesFirst.iterator();
		
		while(iterate.hasNext()) { 
			System.out.println(iterate.next());
		}
		
		System.out.println("======");
		
		// Note that an enhanced for can also loop through the ArrayList.
		// But, while legal, it's probably best to use Iterator.
		
		for(String S : demo.namesFirst) { 
			System.out.println(S);
		}

	}

}
