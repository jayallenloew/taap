/*
 * Copyright Progressive Leasing LLC.
 */
package tapp.common;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.testng.Assert;
import org.testng.annotations.Test;

import taap.common.HelloWorld;

/**
 * Example: A very simple, local test.
 * <p>
 * Since this test doesn't spawn a browser at all, it is atypically fast.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 * @see scr.main.java.tapp.common.HelloWorld.java
 */
public class HelloWorldTest {

	private static final String EXPECTED = 
			"Bonjour Monde" + System.getProperty("line.separator");

	/**
	 * Temporarily redirect output, call a method that creates output, 
	 * then compare actual with expected.
	 * <p>
	 * You may want to keep this in mind for any test case of yours 
	 * which would require you to test the actual output written 
	 * to the console by your application under test.
	 */
	@Test
	public void testHelloWorldOutput() {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		System.setOut(new PrintStream(out));
		HelloWorld hello = new HelloWorld();
		hello.sayHello();
		Assert.assertEquals(HelloWorldTest.EXPECTED, out.toString());
	}

}
