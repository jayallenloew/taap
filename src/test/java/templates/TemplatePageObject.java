package templates;

import java.util.HashSet;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utilities.WebElementGetter;

/**
 * A properly designed page object as an example, using the Luma storefront landing page.
 * <p>
 * This is a template/example. It contains a single WebElement getter. A real 
 * page object could also contain a helper method or two, for the most commonly 
 * required happy-path activity. For instance, a login page would have a 
 * void (or boolean) method with a signature like this:
 * <p>
 * <pre>public void logInHappy(String user, String pass)</pre>
 * </p>
 * For more information:
 * <p>
 * https://progfin.atlassian.net/wiki/spaces/QA/pages/342951580/Creating+Page+Objects+Worth+Using
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @author <a href="mailto:christopher.rains@progleasing.com">CJ Rains</a>
 * @version 2.0 2019-05-16
 */
public class TemplatePageObject {
	
	private WebDriver driverIn;
	
	public TemplatePageObject(WebDriver driverIn) { 
		this.driverIn = driverIn;
	}
	
	/**
	 * Fetch an instance of org.openqa.selenium.WebElement representing the 'Find out more' button 
	 * at bottom: "No Credit Needed <BR> Find out more"
	 * <p>
	 * This supports a typical (not required) clickstream for testers: land here on the 
	 * store demo page, click Find out more to enter the Progressive flow.
	 * <p>
	 * @return An instance of org.openqa.selenium.WebElement
	 */
	public WebElement getButtonFindOutMore() { 
		
		String methodNameLocal = new Exception().getStackTrace()[0].getMethodName();
		
		Set<By> locators = new HashSet<By>();
		locators.add(By.cssSelector("img[alt=\"Learn more about Progressive Leasing\"]"));
		locators.add(By.cssSelector("#prog-valprop > img"));
		locators.add(By.xpath("//a[@id='prog-valprop']/img"));
		locators.add(By.xpath("//img[@alt='Learn more about Progressive Leasing']"));
		
		return WebElementGetter.fetchThisElement(methodNameLocal, driverIn, locators);
	}

}
