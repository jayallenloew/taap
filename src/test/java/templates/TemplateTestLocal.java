package templates;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import common.ProgressiveLeasingConstants;
import pages.BasicInfoPage;
import utilities.TestBaseLocal;

/**
 * Optional template: TestNG test class, local driver, non-static setup.
 * <p>
 * Local driver type is either inherited from the test base, or overridden.
 * Either way, you get a ready-to-use local driver of a specified type.
 * <p>
 * Local means local, so whatever browser you intend to use, make sure it's 
 * installed on your machine. And update src/main/java/utilities.LocalResources.java
 * <p>
 * Because this is a template, I've used more comments and white space 
 * than I otherwise might.
 * <p>
 * For the local drivers, it's typically efficient to do initial 
 * instantiation, navigation, and waiting in a non-static setup.
 * <p>
 * @author <a href="mailto:jay.loew@progleasing.com">Allen</a>
 * @version 1.0
 * @see TestBaseLocal.java
 * @see LocalResources.java
 */
public class TemplateTestLocal extends TestBaseLocal {

	private WebDriver driver;
	private BasicInfoPage pageUnderTest;

	@BeforeMethod
	public void setUpNotStatic() {
		
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		
		/*
		 * Optionally, you can make this call to super first, overriding 
		 * the currently declared default in the test base.
		 */
//		super.setDriverTypeForTestClass(DriverTypeLocal.CHROME);
		
		/*
		 * The line below will fetch a local driver of whatever type is 
		 * currently active in the superclass TestBaseLocal.java, and 
		 * if you haven't explicitly set something above.
		 */
		driver = getPreparedDriver();
		
		/*
		 * Initial navigation required of test methods in this test class.
		 */
		driver.navigate().to(ProgressiveLeasingConstants.URL_BASIC_INFO_PAGE_12);
		
		/*
		 * Defensive programming for Magento and test environments.
		 * Wait for an expected field.
		 */
		try { 
			new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.id("first-name-input")));
		}catch(TimeoutException tE) { 
			throw new IllegalStateException(methodNameLocal + ":\tfast-fail from setup", tE);
		}

		/*
		 * Now that it's safe to do so, instantiate the page object these 
		 * tests will need.
		 */
		pageUnderTest = new BasicInfoPage(driver);
		System.out.println("\n" + methodNameLocal + ":\tsetup complete...");
	}


	/**
	 * Test something.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testSomething(Method method) {

		String testNameLocal = getClass().getSimpleName() + "." + method.getName();

		// do something with the page object
		pageUnderTest.enterSSN("551", "98", "1998");

		// do something with the utilities (optional helper methods)
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldNameLast().click();

		FieldValidationUtilities.sleepSecond(testNameLocal);

		WebElement checkmarkTemp = pageUnderTest.getSSNHappyCheckbox();

		try { 
			Assert.assertTrue(checkmarkTemp.isDisplayed());
			System.out.println(testNameLocal + ":\tPASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + "FAIL - expected green checkmark is missing");
			throw aE;
		}finally { 
			checkmarkTemp = null;
			FieldValidationUtilities.refreshBetweenTest(testNameLocal,driver);
			testNameLocal = null;
		}
	}



	/**
	 * Test something else.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true)
	public void testSomethingElse(Method method) {

		String testNameLocal = getClass().getSimpleName() + "." + method.getName();

		// do something with the page object
		pageUnderTest.enterSSN("551", "98", "1998");

		// do something with the utilities (optional helper methods)
		FieldValidationUtilities.sleepQuarterSecond(testNameLocal);

		pageUnderTest.getFieldNameLast().click();

		FieldValidationUtilities.sleepSecond(testNameLocal);

		WebElement checkmarkTemp = pageUnderTest.getSSNHappyCheckbox();

		try { 
			Assert.assertTrue(checkmarkTemp.isDisplayed());
			System.out.println(testNameLocal + ":\tPASS");
		}catch(AssertionError aE) { 
			System.out.println("\n" + testNameLocal + "FAIL - expected green checkmark is missing");
			throw aE;
		}finally { 
			checkmarkTemp = null;
			FieldValidationUtilities.refreshBetweenTest(testNameLocal,driver);
			testNameLocal = null;
		}
	}
	

	/**
	 * If subclass of TestBaseSauce, pass the test result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit driver.
	 * <p>
	 * Delete the cookie this test just created.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) { 
			System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
			((JavascriptExecutor) driver).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
		} else { 
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		driver.quit();
		System.out.println(methodNameLocal + ":\tEND tearDown.");
	}

}
