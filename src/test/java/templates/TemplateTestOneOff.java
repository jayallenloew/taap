/*
 * Copyright Progressive Leasing LLC.
 */
package templates;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import common.ProgressiveLeasingConstants;
import utilities.CommonDriversLocal;
import utilities.DriverTypeLocal;

/**
 * Example: Simple front end tests with Selenium. Local test execution.
 * <p>
 * This doesn't use a test base; it's a simple, stand-alone, one-off test.
 * <p>
 * @author <a href="mailto:christopher.rains@progleasing.com">CJ Rains</a>
 * @version 2.0 2019-05-16
 */
public class TemplateTestOneOff {

	private WebDriver driver;

	@BeforeMethod
	public void setup() { 
		driver = new CommonDriversLocal(DriverTypeLocal.CHROME).getPreparedDriver();
	}

	@Test
	public void testHappyBankInfo() throws InterruptedException {

		String testNameLocal = new Exception().getStackTrace()[0].getMethodName();	

		driver.navigate().to(ProgressiveLeasingConstants.URL_WELCOME_13);

		try { 
			new WebDriverWait(driver, 10).until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[@type='cta']")));
		}catch(Exception anyException) { 
			// could be timeout or could be not found...
			throw new AssertionError(testNameLocal, anyException);
		}finally { 
			testNameLocal = null;
		}	  
	}

	@AfterMethod
	public void teardown() { 
		driver.quit();
	}
}
