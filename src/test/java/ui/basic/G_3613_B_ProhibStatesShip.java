package ui.basic;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import fieldvalidations.contactinfopage.FV_ContactInfoTests;
import pages.ContactInfoPage;
import utilities.saucelabs.DriverTypeSauce;
import utilities.saucelabs.TestBaseSauce;

/**
 * TestLink path:
 * <p>
 * General | Running | ECOM_UI | ECOM_BasicBeginToResults | G-3613:Ship To Address State
 * <p>
 * *** THIS IS PART 2 of 2 ***
 * <p>
 * Customers in CO, HI, IA, NE, SC, and WY should see an error message.
 * <p>
 * We can't ship leased products to these states.
 * <p>
 * PART 1 (G_3613_A_ProhibStatesLease) concerns leasing:<br> 
 * we can't create a lease for customers in those states.
 * <p>
 * PART 2 (this) concerns shipping:<br>
 * we can't ship leased product to customers in those states.
 * <p>
 * NOTE: Wyoming must be tested manually.
 * <p>
 * Version 1.0 2019-05-15
 * <p>
 * <a href="https://progfin.atlassian.net/browse/ECOM-958">ECOM-958</a>
 * <p>
 * @author <a href="mailto:jay.loew@progleasing.com">Allen</a>
 * @author <a href="mailto:shawn.hartke@progleasing.com">Shawn</a>
 * @author <a href="mailto:christopher.rains@progleasing.com">CJ Rains</a>
 * @version 1.0
 * @see G_3613_A_ProhibStatesLease
 */
public class G_3613_B_ProhibStatesShip extends TestBaseSauce {


	private WebDriver driver;
	private DriverTypeSauce driverType;
	private ContactInfoPage pageUnderTest;
	/**
	 * The wording may seem unusual but I did confirm with business.
	 */
	public static final String messageBase = 
			"Unfortunately, we are unable to lease products to customers residing in ";


	private void setUpNotStatic(String testNameIn) {

		driverType = DriverTypeSauce.MAC_CHROME;

		super.setDriverTypeForTestClass(driverType);

		driver = getPreparedDriver(testNameIn);

		driver.navigate().to(FV_ContactInfoTests.getStartingURL());

		FV_ContactInfoTests.commonWaitInSetup(driver);

		pageUnderTest = new ContactInfoPage(driver);		
	}


	@Test(enabled=true)
	public void testProhibitedShipCO() {

		String testNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();

		setUpNotStatic(testNameLocal);

		String[] CO = { "195 Mountain View","Monument","CO","80132","Colorado" };

		try { 
			Assert.assertTrue(testProhibitedState(CO));
		}catch(AssertionError aE) { 
			throw aE;
		}
	}


	@Test(enabled=true)
	public void testProhibitedShipHI() {

		String testNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();

		setUpNotStatic(testNameLocal);

		String[] HI = { "466 Kinoole St","Hilo","HI","96720","Hawaii" };

		try { 
			Assert.assertTrue(testProhibitedState(HI));
		}catch(AssertionError aE) { 
			throw aE;
		}
	}


	@Test(enabled=true)
	public void testProhibitedShipIA() {

		String testNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();

		setUpNotStatic(testNameLocal);

		String[] IA = { "31555 Central Ave","Dubuque","IA","52001","Iowa" };

		try { 
			Assert.assertTrue(testProhibitedState(IA));
		}catch(AssertionError aE) { 
			throw aE;
		}
	}


	@Test(enabled=true)
	public void testProhibitedShipNE() {

		String testNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();

		setUpNotStatic(testNameLocal);

		String[] NE = { "3820 30th Ave","Kearney","NE","68845","Nebraska" };

		try { 
			Assert.assertTrue(testProhibitedState(NE));
		}catch(AssertionError aE) { 
			throw aE;
		}
	}


	@Test(enabled=true)
	public void testProhibitedShipSC() {

		String testNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();

		setUpNotStatic(testNameLocal);

		String[] SC = { "3250 Airport Blvd","West Columbia","SC","29170","South Carolina" };

		try { 
			Assert.assertTrue(testProhibitedState(SC));
		}catch(AssertionError aE) { 
			throw aE;
		}
	}

	
	private boolean testProhibitedState(String[] addressUnderTest) {

		String testNameLocal = addressUnderTest[4];

		boolean isPassLocal = false;

		super.setDriverTypeForTestClass(driverType);

		pageUnderTest.getFieldAddressLine1().sendKeys(addressUnderTest[0]);
		pageUnderTest.getFieldCity().sendKeys(addressUnderTest[1]);
		pageUnderTest.getFieldState().sendKeys(addressUnderTest[2]);
		
		JavascriptExecutor jsEx = (JavascriptExecutor) driver;
		jsEx.executeScript("window.scrollBy(0,800)");
		jsEx = null;

		pageUnderTest.getFieldZIP().sendKeys(addressUnderTest[3]);
		FieldValidationUtilities.sleepSecond(testNameLocal);
		pageUnderTest.acceptSpam();
		FieldValidationUtilities.sleepSecond(testNameLocal);
		
		WebElement elementTemp = driver.findElement(By.id("no-shipping-errorKey"));
		String captured = elementTemp.getText();
		String expected = messageBase + addressUnderTest[4] + ".";
		
		if(captured.equals(expected)) { 
			isPassLocal = true;
			System.out.println("\n" + testNameLocal + ":\tPASS");
		} else { 
			System.out.println(testNameLocal + ":\tFAIL:");
			System.out.println("Expected \'" + expected + "\'");
			System.out.println("Captured \'" + captured + "\'");
		}
		elementTemp = null;
		captured = null;
		expected = null;
		testNameLocal = null;
		return isPassLocal;
	}


	/**
	 * If subclass of TestBaseSauce, pass the test result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit driver.
<<<<<<< HEAD
	 * <p>
	 * Delete the cookie this test just created.
=======
>>>>>>> master
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		pageUnderTest = null;
		SessionId session = ((RemoteWebDriver)driver).getSessionId();
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) { 
			System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
			// could be null in a happy path run
			if(!(null==session)) { // don't use .equals() here
				((JavascriptExecutor) driver).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
			}
		} else { 
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		if(!(null==driver)) { 
			driver.quit();
		}
		System.out.println(methodNameLocal + ":\tEND tearDown.");
	}

}
