package utilities;

import org.testng.annotations.Factory;

/**
 * This factory instantiates a test with each currently supported driver type.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 * @see TestBaseSauce.java
 * @see TesetBaseLocalTests.java
 */
public class TestBaseLocalTestsFactory {
	
	@Factory
	public Object[] testLocalDrivers() { 
		return new Object[] { 
			new TestBaseLocalTests(DriverTypeLocal.CHROME),
			new TestBaseLocalTests(DriverTypeLocal.CHROME_HEADLESS),
			new TestBaseLocalTests(DriverTypeLocal.FIREFOX),
			new TestBaseLocalTests(DriverTypeLocal.FIREFOX_HEADLESS)
		};
	}

}
