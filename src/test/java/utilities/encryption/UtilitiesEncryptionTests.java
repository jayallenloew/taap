package utilities.encryption;

import org.testng.Assert;
import org.testng.annotations.Test;

public class UtilitiesEncryptionTests {

	/**
	 * Pass in readable value, then validate its encrypted equivalent.
	 * <p>
	 * Notes to testers:
	 * <ul>
	 * <li>If you are creating your encryption key for the first time, 
	 * you will need to change the expected String below.</li>
	 * <li>Then change it again if you update that key.</li>
	 * </ul>
	 */
	@Test(enabled=true,priority=1)
	public void testEncryption1() {

		// 1. Instantiate the encryption class.
		B_Encrypt encr = new B_Encrypt();

		// 2. Pass in readable value to be encrypted.
		String city = encr.getThisValueEncrypted("Strasbourg");
		String expected = "lTtJh+ln7GCQnLwRLOZRuA==";
		
		// 3. Ensure that the value is indeed encrypted.
		try { 
			Assert.assertEquals(city, expected);
		}catch(AssertionError aE) { 
			throw aE;
		}finally { 
			encr	 = null;
			city	 = null;
			expected = null;
		}

	}

	/**
	 * Pass in readable value, then validate its encrypted equivalent.
	 * <p>
	 * Notes to testers:
	 * <ul>
	 * <li>If you are creating your encryption key for the first time, 
	 * you will need to change the expected String below.</li>
	 * <li>Then change it again if you update that key.</li>
	 * </ul>
	 */
	@Test(enabled=true,priority=2)
	public void testEncryption2() {

		// 1. Instantiate the encryption class.
		B_Encrypt encr = new B_Encrypt();

		// 2. Pass in readable value to be encrypted.
		String city = encr.getThisValueEncrypted("Golden Retriever");
		String expected = "nre+H0/0qps9wpA/fPYpuYWkXACAEhIp556ppzlUYOM=";

		// 3. Ensure that the value is indeed encrypted.
		try { 
			Assert.assertEquals(city, expected);
		}catch(AssertionError aE) { 
			throw aE;
		}finally { 
			encr	 = null;
			city	 = null;
			expected = null;
		}
	}
	
	/**
	 * Value is too short; expect an IllegalArgumentException
	 * <p>
	 * Test will pass if IAE is thrown from the class under test and caught here.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true,priority=3,expectedExceptions=IllegalArgumentException.class)
	public void testEncryptionNegative1() { 

		// 1. Instantiate the encryption class.
		B_Encrypt encr = new B_Encrypt();

		// 2. Pass in a known illegal (in this case too short) value
		encr.getThisValueEncrypted("Foo");
	}
	
	/**
	 * Value is null; expect an IllegalArgumentException
	 * <p>
	 * Test will pass if IAE is thrown from the class under test and caught here.
	 * <p>
	 * @param method
	 */
	@Test(enabled=true,priority=3,expectedExceptions=IllegalArgumentException.class)
	public void testEncryptionNegative2() { 

		// 1. Instantiate the encryption class.
		B_Encrypt encr = new B_Encrypt();

		// 2. Pass in a known illegal (in this case null) value
		encr.getThisValueEncrypted(null);
	}


}
