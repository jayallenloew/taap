package utilities.saucelabs;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import common.ProgressiveLeasingConstants;

public class SauceDeviceTests extends TestBaseSauce {

	private CommonDriverSauceDevice cdsd;
	private RemoteWebDriver driverUnderTest;
	
	private static final String uRLForTest = ProgressiveLeasingConstants.URL_BANK_INFO_12;


	@Test(enabled=true)
	public void test_IPHONE_X_RD() {
		String testNameLocal = getClass().getSimpleName() + "." + 
				new Exception().getStackTrace()[0].getMethodName();

		Assert.assertTrue(testEngine(testNameLocal, new CommonDriverSauceDevice(DriverTypeSauce.IPHONE_X_RD)));
	}

	@Test(enabled=true)
	public void test_IPHONE_6P_RD() {
		String testNameLocal = getClass().getSimpleName() + "." + 
				new Exception().getStackTrace()[0].getMethodName();

		Assert.assertTrue(testEngine(testNameLocal, new CommonDriverSauceDevice(DriverTypeSauce.IPHONE_6SP_RD)));
	}

	@Test(enabled=true)
	public void test_IPHONE_7_RD() {
		String testNameLocal = getClass().getSimpleName() + "." + 
				new Exception().getStackTrace()[0].getMethodName();

		Assert.assertTrue(testEngine(testNameLocal, new CommonDriverSauceDevice(DriverTypeSauce.IPHONE_7_RD)));
	}

	@Test(enabled=true)
	public void test_IPHONE_8_RD() {
		String testNameLocal = getClass().getSimpleName() + "." + 
				new Exception().getStackTrace()[0].getMethodName();

		Assert.assertTrue(testEngine(testNameLocal, new CommonDriverSauceDevice(DriverTypeSauce.IPHONE_8_RD)));
	}

	@Test(enabled=true)
	public void test_IPAD_AIR_3_RD() {
		String testNameLocal = getClass().getSimpleName() + "." + 
				new Exception().getStackTrace()[0].getMethodName();

		Assert.assertTrue(testEngine(testNameLocal, new CommonDriverSauceDevice(DriverTypeSauce.IPAD_MINI_2019_RD)));
	}

	@Test(enabled=true)
	public void test_GLXY_NOTE_9_RD() {
		String testNameLocal = getClass().getSimpleName() + "." + 
				new Exception().getStackTrace()[0].getMethodName();

		Assert.assertTrue(testEngine(testNameLocal, new CommonDriverSauceDevice(DriverTypeSauce.GLXY_NOTE_9_RD)));
	}

	@Test(enabled=true)
	public void test_GLXY_S10e_RD() {
		String testNameLocal = getClass().getSimpleName() + "." + 
				new Exception().getStackTrace()[0].getMethodName();

		Assert.assertTrue(testEngine(testNameLocal, new CommonDriverSauceDevice(DriverTypeSauce.GLXY_S10e_RD)));
	}

	@Test(enabled=true)
	public void test_GGL_PXL_3_XL_RD() {
		String testNameLocal = getClass().getSimpleName() + "." + 
				new Exception().getStackTrace()[0].getMethodName();

		Assert.assertTrue(testEngine(testNameLocal, new CommonDriverSauceDevice(DriverTypeSauce.GGL_PXL_3_XL_RD)));
	}


	/**
	 * Pass the result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit the driver.
	 * <p>
	 * There is no local option; these are all mobile tests. All of our 
	 * mobile tests currently run in the Sauce Labs cloud.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {

		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		String testObjDeviceCaptured = driverUnderTest.getCapabilities().getCapability("testobject_device").toString();
		SessionId session = ((RemoteWebDriver)driverUnderTest).getSessionId();

		// begin Sauce teardown
		if(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce")) { 

			System.out.println("\n" + methodNameLocal + ":\tSauce type teardown begin...\n");
			try { 
				Thread.sleep(250);
			}catch(InterruptedException iE) { 
				// deliberate swallow
			}

			// real device 
			if(testObjDeviceCaptured.contains("real")) { 
				System.out.println("\n" + methodNameLocal + ":\ttestObjDeviceCaptured:" + testObjDeviceCaptured + "\n");
				System.out.println("\n" + methodNameLocal + ":\treal device teardown begin...\n");
				try { 
					Thread.sleep(250);
				}catch(InterruptedException iE) { 
					// deliberate swallow
				}
				ResultReporter reporter = new ResultReporter();
				boolean isPass = result.isSuccess();
				String sessionIdLocal = session.toString();
				System.out.println("\n" + methodNameLocal + ":\tabout to send this to reporter:");
				System.out.println("\tsessionIdLocal: " + sessionIdLocal);
				System.out.println("\tisPass: " + isPass + "\n");
				try { 
					Thread.sleep(1000);
				}catch(InterruptedException iE) { 
					// deliberate swallow
				}

				reporter.saveTestStatus(sessionIdLocal, isPass);

				try { 
					Thread.sleep(1000);
				}catch(InterruptedException iE) { 
					// deliberate swallow
				}
				//				reporter = null;
				//				sessionIdLocal = null;

				// emulator or desktop	
			} else { 
				System.out.println("\n" + methodNameLocal + ":\ttestObjDeviceCaptured:" + testObjDeviceCaptured + "\n");
				System.out.println(methodNameLocal + ":\temulator or desktop teardown begin...");
				// could be null in a happy path run
				if(!(null==session)) { // don't use .equals() here
					((JavascriptExecutor) driverUnderTest).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
				}
			}
			// end Sauce teardown

			// begin local teardown
		}else { 
			System.out.println("\n" + methodNameLocal + ":\ttestObjDeviceCaptured:" + testObjDeviceCaptured + "\n");
			System.out.println(methodNameLocal + ":\tLocal driver type detected; Sauce-specific teardown skipped intentionally.\n");
		}
		// end local teardown

		// all teardown
		if(!(null==driverUnderTest)) { 
			System.out.println("driver quit....");
			try { 
				Thread.sleep(5000);
			}catch(InterruptedException iE) { 
				// deliberate swallow
			}

			driverUnderTest.quit();
		}
	}
	

	private boolean testEngine(String testNameIn, CommonDriverSauceDevice cdsd) { 
		this.cdsd = cdsd;
		cdsd.setNameOfCallingTest(testNameIn);
		this.driverUnderTest = this.cdsd.getPreparedDriver();
		boolean isPassLocal = false;
		driverUnderTest.navigate().to(SauceDeviceTests.uRLForTest);
		try { 
			new WebDriverWait(driverUnderTest, 60).until(ExpectedConditions.presenceOfElementLocated(By.id("input-account-number")));
		}catch(TimeoutException tE) { 
			driverUnderTest.quit();
			throw new AssertionError(tE);
		}

		WebElement fieldUnderTest = driverUnderTest.findElement(By.id("input-account-number"));

		try { // type
			fieldUnderTest.sendKeys("123456"); // phoney account number
			FieldValidationUtilities.sleepSecond(testNameIn);
		}catch(Exception anyExceptionFailsHere) { 
			driverUnderTest.quit();
			throw new AssertionError(testNameIn,anyExceptionFailsHere);
		}

		try { // clear -- and we're good, last one
			fieldUnderTest.clear(); 
			FieldValidationUtilities.sleepSecond(testNameIn);
			System.out.println("\n" + testNameIn + ":\tPASS\n");
			isPassLocal = true;
		}catch(Exception anyExceptionFailsHere) { 
			System.out.println("\n" + testNameIn + ":\tFAIL catching " + anyExceptionFailsHere.getClass().getSimpleName() + "\n");
			// deliberate swallow
			// in which case this method will return false and 
			// the calling test will throw AssertionError
		}finally { 
			fieldUnderTest = null;
		}

		return isPassLocal;
	}

}
