package utilities.saucelabs;

import java.lang.reflect.Method;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import common.ProgressiveLeasingConstants;

/**
 * Minimalist unit test for new base class for Sauce Labs drivers.
 * <p>
 * Version 1.1 2019-05-28 parameterize variable and streamline teardown.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.1
 * @see TestBaseSauce.java
 * @see TestBaseSauceDesktopFactory.java
 */
public class TestBaseSauceDesktop extends TestBaseSauce { 
	
	@SuppressWarnings("unused")
	private DriverTypeSauce driverType;
	
	private CommonDriverSauceDesktop cds;
	private WebDriver driverUnderTest;
	private String testNameIn;
	
	private static final String getStartingURL() { 
		return ProgressiveLeasingConstants.URL_BANK_INFO_13;
	}	
	
	private static final DataCenter getDataCenter() { 
		return DataCenter.USA;
	}	

	public TestBaseSauceDesktop(DriverTypeSauce driverType) { 
		this.driverType = driverType;
		cds = new CommonDriverSauceDesktop(driverType, TestBaseSauceDesktop.getDataCenter());	
		testNameIn = driverType.name();
	}
	
	

	@Test
	public void testSauceDriver(Method method) {
		cds.setNameOfCallingTest(testNameIn);
		driverUnderTest = cds.getPreparedDriver();
		driverUnderTest.navigate().to(TestBaseSauceDesktop.getStartingURL());
		try { 
			new WebDriverWait(driverUnderTest, 15).until(ExpectedConditions.presenceOfElementLocated(By.id("input-account-number")));
		}catch(TimeoutException tE) { 
			driverUnderTest.quit();
			throw new AssertionError(method.getName(),tE);
		}
		
		WebElement fieldUnderTest = driverUnderTest.findElement(By.id("input-account-number"));
		
		try { // type
			fieldUnderTest.sendKeys("123456"); // phoney account number
			FieldValidationUtilities.sleepSecond(method.getName());
		}catch(Exception anyExceptionFailsHere) { 
			driverUnderTest.quit();
			throw new AssertionError(method.getName(),anyExceptionFailsHere);
		}

		try { // clear -- and we're good, last one
			fieldUnderTest.clear(); 
			FieldValidationUtilities.sleepSecond(method.getName());
		}catch(Exception anyExceptionFailsHere) { 
			throw new AssertionError(method.getName(),anyExceptionFailsHere);
		}finally { 
			fieldUnderTest = null;
		}
	}
	
	/**
	 * Pass the result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit the driver.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {
		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		SessionId session = ((RemoteWebDriver)driverUnderTest).getSessionId();
		if(!(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce"))) { 
			throw new IllegalStateException(getClass().getSimpleName() + "." + methodNameLocal + ":\tthis requires a Sauce driver type");
		}
		System.out.println(methodNameLocal + ":\tSauce driver type detected; Sauce-specific teardown up next...");
		// could be null in a happy path run
		if(!(null==session)) { // don't use .equals() here
			((JavascriptExecutor) driverUnderTest).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
		}
		if(!(null==driverUnderTest)) { 
			driverUnderTest.quit();
		}
	}

	
	
}