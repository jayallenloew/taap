package utilities.saucelabs;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.SessionId;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import common.FieldValidationUtilities;
import common.ProgressiveLeasingConstants;

/**
 * This test class receives instances from a test factory.
 * <p>
 * Version 1.0 2019-05-21
 * <p>
 * Version 1.1 2019-05-28 parameterize variable and streamline teardown.
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.1
 */
public class TestBaseSauceDevice extends TestBaseSauce { 


	private DriverTypeSauce driverType;
	private CommonDriverSauceDevice cdsd;
	private RemoteWebDriver driverUnderTest;

	private static final String getStartingURL() { 
		return ProgressiveLeasingConstants.URL_BANK_INFO_12;
	}
	
	public TestBaseSauceDevice(DriverTypeSauce driverType) { 
		this.driverType = driverType;
		this.cdsd = new CommonDriverSauceDevice(this.driverType);	
		this.driverUnderTest = this.cdsd.getPreparedDriver();
	}


	@Test
	public void testSauceDriverRealDevice() {
		String testNameLocal = new Exception().getStackTrace()[0].getMethodName();
		cdsd.setNameOfCallingTest(testNameLocal);
		driverUnderTest.navigate().to(TestBaseSauceDevice.getStartingURL());
		try { 
			new WebDriverWait(driverUnderTest, 60).until(ExpectedConditions.presenceOfElementLocated(By.id("input-account-number")));
		}catch(TimeoutException tE) { 
			driverUnderTest.quit();
			throw new AssertionError(tE);
		}

		WebElement fieldUnderTest = driverUnderTest.findElement(By.id("input-account-number"));

		try { // type
			fieldUnderTest.sendKeys("123456"); // phoney account number
			FieldValidationUtilities.sleepSecond(testNameLocal);
		}catch(Exception anyExceptionFailsHere) { 
			driverUnderTest.quit();
			throw new AssertionError(testNameLocal,anyExceptionFailsHere);
		}

		try { // clear -- and we're good, last one
			fieldUnderTest.clear(); 
			FieldValidationUtilities.sleepSecond(testNameLocal);
			System.out.println("\n" + testNameLocal + ":\tPASS\n");
		}catch(Exception anyExceptionFailsHere) { 
			System.out.println("\n" + testNameLocal + ":\tFAIL catching " + anyExceptionFailsHere.getClass().getSimpleName() + "\n");
			throw new AssertionError(testNameLocal,anyExceptionFailsHere);
		}finally { 
			fieldUnderTest = null;
		}
	}

	/**
	 * Pass the result up to the Sauce Labs Dashboard.
	 * <p>
	 * Quit the driver.
	 * <p>
	 * There is no local option; these are all mobile tests. All of our 
	 * mobile tests currently run in the Sauce Labs cloud.
	 */
	@AfterMethod
	public void tearDown(ITestResult result) throws Exception {

		String methodNameLocal = getClass().getSimpleName() + "." + new Exception().getStackTrace()[0].getMethodName();
		String testObjDeviceCaptured = driverUnderTest.getCapabilities().getCapability("testobject_device").toString();
		SessionId session = ((RemoteWebDriver)driverUnderTest).getSessionId();

		// begin Sauce teardown
		if(!(this.getClass().getSuperclass().getSimpleName().equals("TestBaseSauce"))) { 
			throw new IllegalStateException(getClass().getSimpleName() + "." + methodNameLocal + ":\tthis requires a Sauce driver type");
		}

		System.out.println("\n" + methodNameLocal + ":\tSauce type teardown begin...\n");
		try { 
			Thread.sleep(5000);
		}catch(InterruptedException iE) { 
			// deliberate swallow
		}

		// real device 
		if(testObjDeviceCaptured.contains("real")) { 
			System.out.println("\n" + methodNameLocal + ":\treal device teardown begin...\n");
			try { 
				Thread.sleep(5000);
			}catch(InterruptedException iE) { 
				// deliberate swallow
			}
			ResultReporter reporter = new ResultReporter();
			boolean isPass = result.isSuccess();
			String sessionIdLocal = session.toString();
			System.out.println("\n" + methodNameLocal + ":\tabout to send this to reporter:");
			System.out.println("\tsessionIdLocal: " + sessionIdLocal);
			System.out.println("\tisPass: " + isPass + "\n");
			try { 
				Thread.sleep(5000);
			}catch(InterruptedException iE) { 
				// deliberate swallow
			}
			reporter.saveTestStatus(sessionIdLocal, isPass);
			reporter = null;
			sessionIdLocal = null;

			// emulator or desktop	
		} else { 
			System.out.println(methodNameLocal + ":\temulator or desktop teardown begin...");
			// could be null in a happy path run
			if(!(null==session)) { // don't use .equals() here
				((JavascriptExecutor) driverUnderTest).executeScript("sauce:job-result=" + (result.isSuccess() ? "passed" : "failed"));
			}
		}
		// end Sauce teardown

		// all teardown
		if(!(null==driverUnderTest)) { 
			System.out.println("driver quit....");
			try { 
				Thread.sleep(5000);
			}catch(InterruptedException iE) { 
				// deliberate swallow
			}
			driverUnderTest.quit();
		}
	}
}