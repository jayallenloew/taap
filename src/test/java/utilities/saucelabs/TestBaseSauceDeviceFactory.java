package utilities.saucelabs;

import org.testng.annotations.Factory;

import utilities.saucelabs.DriverTypeSauce;

/**
 * Deprecated 2019-05-24. Don't run via factory anymore. The device cloud can't handle it. 
 * See instead SauceDeviceTests.java.
 * <p>
 * Version 1.0 2019-05-10
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 * <p>
 * @deprecated 
 * <p>
 * @see SauceDeviceTests.java
 */
public class TestBaseSauceDeviceFactory {
	
	@Factory
	public Object[] testMobileDevices() { 
		return new Object[] { 
				new TestBaseSauceDevice(DriverTypeSauce.IPHONE_X_RD),
				new TestBaseSauceDevice(DriverTypeSauce.IPHONE_6SP_RD),
				new TestBaseSauceDevice(DriverTypeSauce.IPHONE_7_RD),
				new TestBaseSauceDevice(DriverTypeSauce.IPHONE_8_RD),
				new TestBaseSauceDevice(DriverTypeSauce.IPAD_MINI_2019_RD),
				new TestBaseSauceDevice(DriverTypeSauce.GLXY_NOTE_9_RD),
				new TestBaseSauceDevice(DriverTypeSauce.GLXY_S10e_RD),
				new TestBaseSauceDevice(DriverTypeSauce.GGL_PXL_3_XL_RD)
		};
	}

}
