package utilities.saucelabs;

import org.testng.annotations.Factory;

import utilities.saucelabs.DriverTypeSauce;

/**
 * This factory instantiates instances of different mobile device types.
 * <p>
 * Version 1.0 2019-05-10
 * <p>
 * @author <a href="mailto:Jay.Loew@progleasing.com">Allen Loew</a>
 * @version 1.0
 * @see TestBaseSauceMobile.java
 * @see TestBaseSauceMobile.xml
 */
public class TestBaseSauceMobileFactory {
	
	@Factory
	public Object[] testMobileSimulators() { 
		return new Object[] { 
				new TestBaseSauceMobile(DriverTypeSauce.SAMSUNG_GALAXY_S7_SIM),
				new TestBaseSauceMobile(DriverTypeSauce.IPHONE_6SP_SIM),
				new TestBaseSauceMobile(DriverTypeSauce.IPHONE_8_SIM),
				new TestBaseSauceMobile(DriverTypeSauce.IPHONE_X_SIM),
				new TestBaseSauceMobile(DriverTypeSauce.GOOGLE_PIXEL_C_SIM)
		};
	}

}
